package controller;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Vector;


import model.TableModel;
import model.Position;
import model.Switch;
import model.TunnelEntrance;
import view.DrawPanel;
import view.GameWindow;
import view.GameWindow.LayoutType;
import model.EngineTimer;



/**
 * 
 * @author Toth
 * Az MVC tervezési mintát követve ez az osztály látja el a
 * vezérlő szerepet: rajta keresztül lehet a vezérelhető elemek
 * állapotát változtatni.
 *
 */
public class Controller
{

	/**
	 * Referencia a TableModel-re.
	 */
	private TableModel tablemodel;
	/**
	 * Lista a váltókról.
	 */
	private Map<Position, Switch> switches = new HashMap<Position, Switch>();
	/**
	 * Lista az alagútszájakról.
	 */
	private Map<Position, TunnelEntrance> tunnels = new HashMap<Position, TunnelEntrance>();
	/**
	 * Referencia az EngineTimer-ra.
	 */
	private EngineTimer engineTimer;
	
	private Scanner sc;
	
	private GameWindow gw;
	
	private HashMap<String, Command> cmds;
	
	/**
	 *  A játékos állapotait reprezentáló enum
	 *  */
	private enum PlayerState {
		BeforeGame,
		Game_BeforeStart,
		Game_InGame,
		Game_StoppedGame_GameWon,
		Game_StoppedGame_PausedGame,
		Game_StoppedGame_GameLost,
		Exiting
	}
	/**
	 *  A játékos állapotát reprezentáló változó
	 *  */
	PlayerState playerState = PlayerState.BeforeGame;
	
	/**
	 *  a felugro üzeneteket, melyek jelzik az állapotunkat,
	 *  felsorolját az új állapot lehetséges függvényeit, engedélyezi illetve tiltja.
	 *  */
	boolean setmessages = false;
	
	/**
	 *  az autotic állapotát reprezentálja
	 *  */
	boolean autotic = true;
	
	/**
	 *  a pályán való változások kiíratását engedélyezi illetve tiltja
	 *  */
	static boolean seteventdisplay = true;

	final int levelCount = 2;
	int currentLevel = 1;
	
	
	public Controller()
	{
		/* Parancsok felvétele */
		cmds = new HashMap<String, Command>();
		{	// a cmdVec-re később nincs szükgség, ezért van külön blokkban
			Vector<Command> cmdVec = new Vector<Command>();
			
			cmdVec.add(new Exit());
			cmdVec.add(new Man());
			cmdVec.add(new State());
			cmdVec.add(new SetMessages());
			cmdVec.add(new SetEventDisplay());
			cmdVec.add(new NewGame());
			cmdVec.add(new ListSaves());
			cmdVec.add(new SetAutoTic());
			cmdVec.add(new Tic());
			cmdVec.add(new Start());
			cmdVec.add(new GetSwitch());
			cmdVec.add(new GetTunnel());
			cmdVec.add(new SetSwitch());
			cmdVec.add(new SetTunnel());
			cmdVec.add(new Pause());
			cmdVec.add(new Resume());
			cmdVec.add(new NextGame());
			cmdVec.add(new Retry());
			cmdVec.add(new LoadGame());
			
			for(Command c : cmdVec)
				cmds.put(c.callName(), c);

		} // ide érve a cmds map már megfelelően fel van töltve az összes paranccsal
		
		
		
		sc = new Scanner(System.in);
	}
	
	
	/**
	 * 
	 * @param tablemodel
	 */
	public void setTableModel(TableModel tablemodel)
	{
		this.tablemodel = tablemodel;
	}
	
	public void setGameWindow(GameWindow gamewindow)
	{
		gw = gamewindow;
	}

	/**
	 * A TableModel hívja meg, ha a játékos nyert, vagy veszített. Ez utóbbit a paraméterben lehet átadni.
	 * @param win True, ha a játékos nyert, false, ha veszített.
	 */
	public void gameOver(boolean win)
	{
		if(win) {
			playerState = PlayerState.Game_StoppedGame_GameWon;
			if(currentLevel == levelCount) { // a felhasználó kijátszottta a játékot
				//printStatus("Kijatszottad a jatekot! nextgame parancs eseten elolrol kezdhetetd.");
				//currentLevel = 1;
				
				boolean newgame =  gw.showOKCancelMessageBox("Kijatszottad a jatekot! "
						+ "OK-ra klikkelve elolrol kezdhetetd.");
				if(newgame) {
					playerState = PlayerState.BeforeGame;
					executeCommand("newgame");
				}
			} else {	// még van pálya ez után
				currentLevel++;
				
				boolean nextgame =  gw.showOKCancelMessageBox("Gratulalunk! Kijatszodtad a palyat! "
						+ "Ha szeretned folyatni "
						+ "a kovetkezo palyaval, OK-t nyomj!");
				if(nextgame)
					executeCommand("nextgame");
			}
		} else {
			playerState = PlayerState.Game_StoppedGame_GameLost;
			
			boolean retry =  gw.showOKCancelMessageBox("Sajnos az egyik vonat balesetet szenvedett. "
					+ "OK-ra klikkelve előről kezdheted a pályát.");
			if(retry) {
				switches.clear();
				tunnels.clear();
				executeCommand("retry");
			}
		}
	}

	/**
	 * 
	 * @param switch_
	 */
	public void addSwitch(Switch switch_)
	{
		switches.put(switch_.getPosition(), switch_);
	}

	/**
	 * 
	 * @param tunnelEntrance
	 */
	public void addTunnelEntrance(TunnelEntrance tunnelEntrance)
	{
		tunnels.put(tunnelEntrance.getPosition(), tunnelEntrance);
	}

	/**
	 * 
	 * @param engineTimer
	 */
	public void setEngineTimer(EngineTimer engineTimer)
	{
		this.engineTimer = engineTimer;
	}

	/**
	 * Törli az összes listát (switches, tunnelEntrances), mert pl.: új pályát kezdtünk el.
	 */
	public void ClearAll()
	{
		switches.clear();
		tunnels.clear();
	}

	/**
	 * Az Application hívja meg, olvassa a felhasználó parancsait és annak megfelelően működik.
	 */
	public void play()
	{
		
		
//		while((playerState != PlayerState.Exiting) && sc.hasNextLine()) {
//			String line = sc.nextLine();
//			if(line.isEmpty() == false) {	//ures sorokat nem vesszuk figyelembe
//				String[] args = line.split(" ");
//				String cmdName = args[0];
//				Command cmd = cmds.get(cmdName);
//				if (cmd != null)
//				{
//					if(cmd.executable(playerState)) {
//						cmd.execute(args, cmds);
//					} else {
//						printStatus("Cannot execute this command in current state.");
//					}
//					if(setmessages) {	// kell irogatni, hogy milyen állapotban vagyunk, milyen parancsok vannak stb.
//						cmds.get("state").execute(null, null); 	// kiírja a mostani állapotunkat
//						System.out.print("Elerheto parancsok: ");
//						for(Command c : cmds.values()) {
//							if(c.executable(playerState)) {
//								System.out.print(c.callName() + ", ");
//							}
//						}
//						printStatus("");
//					}
//				}
//				else
//				{
//					printStatus("Unknown command.");
//				}
//
//			}
//		}
//		
//		sc.close();
		
		gw.SetLayout(LayoutType.Menu_Main);
	}

	/**
	 * A paraméterben kapott szöveget - ha a seteventdisplay = true - kiírja a standard kimenetre.
	 * @param msg
	 */
	public static void print(String msg)
	{
		if(seteventdisplay) {
			System.out.println(msg);
		}
	}
	
	/**
	 * A paraméterben kapott szöveget kiírja a standard kimenetre.
	 * @param msg
	 */
	public void printStatus(String msg)
	{
		System.out.println(msg);
	}
	
	
	
	/*--------------------------------- Innentől vannak a privát függvények, privát beágyazott osztályok, interfészek--------------------------*/
	
	
	/**
	 * Interfész a végrehajtható parancsokhoz
	 * */
	private interface Command {
		/**
		 * Vegrehajtja a parancsot.
		 * @param args : a kapott paraméterek, 0. indexű tagja maga a parancs neve
		 * @param cmds : a jatek soran hasznalhato parancsok gyujtemenye
		 * */
		void execute(String[] args, HashMap<String, Command> cmds);

		/**
		 * @return Visszater a parancs hívási nevével.
		 * */
		String callName();
		
		
		/**
		 * @return Visszater a parancs rövid leírásával.
		 * */
		String shortDescritpion();
		
		/**
		 * @return Visszater a parancs részletes leírásával.
		 * */
		String longDescritpion();
		
		/**
		 * @return Visszater azzal, hogy a parancs végrehajtható-e a paraméterben kapott játékos állapotban.
		 * */
		boolean executable(PlayerState ps);
	}
	
	/**
	 * Az exit parancsot reprezentáló osztály
	 * BeforeGame allapotban leallitja es bezarja az alkalmazast.
	 *	StoppedGame allapotban kilep a fomenube es atlepunk BeforeGame allapotba.;
	 * */
	private class Exit implements Command {

		@Override
		public void execute(String[] args,  HashMap<String, Command> cmds) {
			playerState = (playerState == PlayerState.BeforeGame) ? PlayerState.Exiting : PlayerState.BeforeGame;
			gw.dispose();
		}

		@Override
		public String callName() {
			return "exit";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.BeforeGame ||
					ps == PlayerState.Game_StoppedGame_GameWon ||
					ps == PlayerState.Game_StoppedGame_PausedGame ||
					ps == PlayerState.Game_StoppedGame_GameLost);
		}

		@Override
		public String shortDescritpion() {
			return "A kilepesert felelos parancs.";
		}

		@Override
		public String longDescritpion() {
					return "BeforeGame allapotban leallitja es bezarja az alkalmazast. \n" + 
					"StoppedGame allapotban kilep a fomenube es atlepunk BeforeGame allapotba.";
		}
		
	}
	
	/**
	 * A man parancsot reprezentáló osztály
	 * 
	 * man [-a]
	 *	Leírás: A játékos aktuális állapotában hívható függvények és rövid leírásuknak a gyűjteményét listázza ki.
	 *	Opció: -a hatására a játékban használható összes lehetséges parancsot és leírását listázzuk ki.
	 *
	 *		man <parancs>
	 *		Leírás: A paraméterként kapott parancs leírását és szintaktikáját, paraméterezését jeleníti meg példakódokkal.
	 *
	 *
	 * */
	private class Man implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			if(args.length == 1) {	// semmi paraméter nem érkezett
				for(Command c : cmds.values()) {
					if(c.executable(playerState)) {
						printStatus(c.callName() + ": " + c.shortDescritpion());
					}
				}
			} else if(args[1].equals("-a")) {	// az összes parancs kiirása kell
				for(Command c : cmds.values()) {
					printStatus(c.callName() + ": " + c.shortDescritpion());
				}
			} else {
				// args[1] tartalmazza a részletezni kivánt parancs nevét
				Command c = cmds.get(args[1]);
				if (c != null)
				{
					printStatus(c.callName() + ": " + c.longDescritpion());
				}
				else
				{
					printStatus("Unknown command.");
				}
			}
		}

		@Override
		public String callName() {
			return "man";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return true;		// ez a parancs mindig végrehajtható
		}

		@Override
		public String shortDescritpion() {
			return "A parancsok manualjat megjelenito parancs.";
		}

		@Override
		public String longDescritpion() {
			return	"man [-a] : A jatekos aktualis allapotaban hivhato fuggvenyek es rovid leirasuknak a gyujtemenyet listazza ki. \n " +
					"Opcio: -a hatasara a jatekban hasznalhato osszes lehetseges parancsot es leirasat listazzuk ki. \n" + 
					"man <parancs> : A parameterkent kapott parancs leirasat es szintaktikajat, parameterezeset jeleniti meg, nehol peldakodokkal, pl.: man start.";
		}
		
	}

	/**
	 * A state parancsot reprezentáló osztály
	 * 
	 *	state
	 *	Leírás: A játékos aktuális állapotát lekérdező parancs.
	 *
	 * */
	private class State implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			String result = "";
			switch(playerState) {
			case BeforeGame:
				result = "BeforeGame";
				break;
			case Exiting:
				result = "Exiting";
				break;
			case Game_BeforeStart:
				result = "Game::BeforeStart";
				break;
			case Game_InGame:
				result = "Game::InGame";
				break;
			case Game_StoppedGame_GameLost:
				result = "Game::StoppedGame::GameLost";
				break;
			case Game_StoppedGame_GameWon:
				result = "Game::StoppedGame::GameWon";
				break;
			case Game_StoppedGame_PausedGame:
				result = "Game::StoppedGame::PausedGame";
				break;
			default:
				break;
			
			}
			printStatus(result);
		}

		@Override
		public String callName() {
			return "state";
		}

		@Override
		public String shortDescritpion() {
			return "A jatekos aktualis allapotat lekerdezo parancs.";
		}

		@Override
		public String longDescritpion() {
			return "A jatekos aktualis allapotat lekerdezo parancs.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return true;	// mindig vegrehajthato
		}
		
	}
	
	/**
	 *setmessages <p>
	 *	Leírás: A felugró üzeneteket, amelyek jelzik az állapotunkat,felsorolják
	 *	az új állapot lehetséges függvényeit, stb. ezzel a paranccsal lehet megszüntetni
	 *	illetve újból engedélyezni a kapott logikai paraméter segítéségével,
	 *	amely értéke 0 vagy 1 kell legyen.
	 *
	 * */
	
	private class SetMessages implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			if(args.length < 2) {
				printStatus("Missing parameter.");
			} else if(args[1].equals("0")) {
				setmessages = false;
			} else if(args[1].equals("1")) {
				setmessages = true;
			} else {
				printStatus("Invalid parameter.");
			}
		}

		@Override
		public String callName() {
			return "setmessages";
		}

		@Override
		public String shortDescritpion() {
			return "A felugro uzeneteket engedelyezi illetve tiltja";
		}

		@Override
		public String longDescritpion() {
			return "setmessages <p>: A felugró üzeneteket, amelyek jelzik az állapotunkat, " + 
		"felsorolják az új állapot lehetséges függvényeit, stb. ezzel a paranccsal lehet" + 
		" megszüntetni illetve újból engedélyezni a kapott logikai paraméter segítéségével, amely értéke 0 vagy 1 kell legyen.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.Game_BeforeStart ||
					ps == PlayerState.Game_InGame ||
					ps == PlayerState.Game_StoppedGame_GameWon ||
					ps == PlayerState.Game_StoppedGame_PausedGame ||
					ps == PlayerState.Game_StoppedGame_GameLost);
		}
		
	}
	
	
	/**
	 *	seteventdisplay <p>
	 *	Leírás: A játék folyamán minden egyes órajel után az elemek a pályán megváltozhatnak
	 *	(pl. vonatok elmozdulnak) és erről alapesetben a felhasználót értesítjük a konzolon.
	 *	Ezt az opciót a p logikai változó segítéségével ki- és be lehet kapcsolni.
	 *
	 * */
	private class SetEventDisplay implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			if(args.length < 2) {
				printStatus("Missing parameter.");
			} else if(args[1].equals("0")) {
				seteventdisplay = false;
			} else if(args[1].equals("1")) {
				seteventdisplay = true;
			} else {
				printStatus("Invalid parameter.");
			}
		}

		@Override
		public String callName() {
			return "seteventdisplay";
		}

		@Override
		public String shortDescritpion() {
			return "A palyan a valtozasok megjeleniteset engedelyezi illetve tiltja.";
		}

		@Override
		public String longDescritpion() {
			return "seteventdisplay <p>: A játék folyamán minden egyes órajel után az elemek a pályán megváltozhatnak " + 
		"(pl. vonatok elmozdulnak) és erről alapesetben a felhasználót értesítjük a konzolon." + 
		" Ezt az opciót a p logikai változó segítéségével ki- és be lehet kapcsolni.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.Game_BeforeStart ||
					ps == PlayerState.Game_InGame ||
					ps == PlayerState.Game_StoppedGame_GameWon ||
					ps == PlayerState.Game_StoppedGame_PausedGame ||
					ps == PlayerState.Game_StoppedGame_GameLost);
		}
		
	}
	
	/**
	 *	newgame 
	 * 	Leírás: Betölti a legelső pályát, de még nem indítja el.
	 * 	Átlépünk Before Start állapotba.
	 *
	 * */
	
	private class NewGame implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			
			currentLevel = 1;	// elölről kezdjük a játékot
			cmds.get("nextgame").execute(args, cmds);	// igénybe vesszük ezt a funkcionalitást, így nincs kód-duplikáció.
		}

		@Override
		public String callName() {
			return "newgame";
		}

		@Override
		public String shortDescritpion() {
			return "Betölti a legelső pályát, de még nem indítja el.";
		}

		@Override
		public String longDescritpion() {
			return "Betölti a legelső pályát, de még nem indítja el. Átlépünk Before Start állapotba.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.BeforeGame);
		}
		
	}
	
	
	/**
	 *	listsaves 
	 *	Leírás: Korábban elmentett játékok neveit lehet kilistázni.
	 *
	 * */
	
	private class ListSaves implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			File dir = new File("saves/");
			File[] filesList = dir.listFiles();
			for (File file : filesList) {
			    if (file.isFile()) {
			        printStatus(file.getName());
			    }
			}
		}

		@Override
		public String callName() {
			return "listsaves";
		}

		@Override
		public String shortDescritpion() {
			return "Korábban elmentett játékok neveit lehet kilistázni.";
		}

		@Override
		public String longDescritpion() {
			return "Korábban elmentett játékok neveit lehet kilistázni.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.BeforeGame);
		}
		
	}
	
	
	/**
	 *	setautotic <p>
	 *	Leírás: az automatikus órajel kapcsolahtó ki vele a p logikai paraméter segítségével.
	 *
	 * */
	private class SetAutoTic implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			if(args.length < 2) {
				printStatus("Missing parameter.");
			} else if(args[1].equals("0")) {
				autotic = false;
				engineTimer.stop();
			} else if(args[1].equals("1")) {
				autotic = true;
				engineTimer.start();
			} else {
				printStatus("Invalid parameter.");
			}
		}

		@Override
		public String callName() {
			return "setautotic";
		}

		@Override
		public String shortDescritpion() {
			return "Az automatikus órajel kapcsolahtó ki vele.";
		}

		@Override
		public String longDescritpion() {
			return "setautotic <p>s: az automatikus órajel kapcsolahtó ki vele a p logikai paraméter segítségével.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.Game_BeforeStart ||
					ps == PlayerState.Game_InGame ||
					ps == PlayerState.Game_StoppedGame_GameWon ||
					ps == PlayerState.Game_StoppedGame_PausedGame ||
					ps == PlayerState.Game_StoppedGame_GameLost);
		}
		
	}
	
	/**
	 * tic 
	 * Leírás: kikapcsolt automatikus órajel esetén ezzel lehet mesterségesen órajelet előidézni.
	 *
	 * */
	private class Tic implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			if(!autotic){
				engineTimer.tick();
			} else {
				printStatus("Autotic = true, nem lehet ezt a parancsot kiadni.");
			}
		}

		@Override
		public String callName() {
			return "tic";
		}

		@Override
		public String shortDescritpion() {
			return "Kikapcsolt automatikus órajel esetén ezzel lehet mesterségesen órajelet előidézni.";
		}

		@Override
		public String longDescritpion() {
			return "Kikapcsolt automatikus órajel esetén ezzel lehet mesterségesen órajelet előidézni.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.Game_InGame);
		}
		
	}
	
	
	
	/**
	 * Hatására a játék ténylegesen elindul. Átlépünk In Game állapotba.
	 */
	private class Start implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			playerState = PlayerState.Game_InGame;
			if(autotic)
				engineTimer.start();
		}

		@Override
		public String callName() {
			return "start";
		}

		@Override
		public String shortDescritpion() {
			return "Hatására a játék ténylegesen elindul.";
		}

		@Override
		public String longDescritpion() {
			return "Hatására a játék ténylegesen elindul. Átlépünk In Game állapotba.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.Game_BeforeStart);
		}
		
	}
	
	
	/**
	 * kiírja a felületre az összes váltó pozicióját (két koordináta),
	 * amely által lehet rájuk hivatkozni.
	 *
	 */
	private class GetSwitch implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			for (Switch s : switches.values()) {
				Position p = s.getPosition();
				printStatus(p.posX + " " + p.posY);
			}
		}

		@Override
		public String callName() {
			return "getswitch";
		}

		@Override
		public String shortDescritpion() {
			return "Kiírja a felületre az összes váltó pozicióját (két koordináta), amely által lehet rájuk hivatkozni.";
		}

		@Override
		public String longDescritpion() {
			return "Kiírja a felületre az összes váltó pozicióját (két koordináta), amely által lehet rájuk hivatkozni.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.Game_InGame);
		}
		
	}
	
	
	/**
	 *  kiírja a felületre az összes potenciális alagútszáj pozicióját
	 *  (két koordináta), amely által lehet rájuk hivatkozni. 
	 */
	
	private class GetTunnel implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			for (TunnelEntrance t : tunnels.values()) {
				Position p = t.getPosition();
				printStatus(p.posX + " " + p.posY);
			}
		}

		@Override
		public String callName() {
			return "gettunel";
		}

		@Override
		public String shortDescritpion() {
			return "kiírja a felületre az összes potenciális alagútszáj pozicióját (két koordináta), amely által lehet rájuk hivatkozni.";
		}

		@Override
		public String longDescritpion() {
			return "Kiírja a felületre az összes potenciális alagútszáj pozicióját (két koordináta), amely által lehet rájuk hivatkozni.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.Game_InGame);
		}
		
	}
	
	/**
	 * setswitch <position> Leírás: A paraméterben megadott pozícióban 
	 * (szóközökkel elválasztott kettő darab természetes szám,
	 * amely megjelent a getswitch által visszaadott listában is)
	 * található váltó állását változtatja meg, ha nincs rajta vonat.
	 * Ellenkező esetben hibaüzenettel tér vissza.
	 */
	
	private class SetSwitch implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			if(args.length < 3) {	// legalább 2 koordináta kell
				printStatus("Invalid parameter count");
			} else {	// a parameterek száma stimmel
				try {
					int x = Integer.parseInt(args[1]);
					int y = Integer.parseInt(args[2]);
					Switch sw = switches.get(new Position(x, y));
					if(sw == null) {	// nincs ilyen váltó
						printStatus("No switch at the given positon");
					} else {	// van ilyen váltó
						sw.switchOutput();
					}
				} catch(NumberFormatException e) {
					printStatus("Invalid parameter");
				}
			}
		}

		@Override
		public String callName() {
			return "setswitch";
		}

		@Override
		public String shortDescritpion() {
			return "A váltó állását lehet vele változtatni";
		}

		@Override
		public String longDescritpion() {
			return "setswitch <position> Leírás: A paraméterben megadott pozícióban " +  
					"(szóközökkel elválasztott kettő darab természetes szám, amely megjelent a getswitch által " +
					"visszaadott listában is) található váltó állását változtatja meg, " + 
					"ha nincs rajta vonat. Ellenkező esetben hibaüzenettel tér vissza.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.Game_InGame);
		}
		
	}
	
	
	/**
	 * settunnel <position> 
	 * Leírás: A paraméterben megadott pozícióban 
	 * (szóközökkel elválasztott kettő darab természetes szám,
	 * amely megjelent a gettunnel által visszaadott listában is)
	 * található alagútszáj állapotát változtatja meg,
	 * ha nem halad át éppen rajta vonat.
	 * Ellenkező esetben hibaüzenettel tér vissza.
	 */
	private class SetTunnel implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			if(args.length < 3) {	// legalább 2 koordináta kell
				printStatus("Invalid parameter count");
			} else {	// a parameterek száma stimmel
				try {
					int x = Integer.parseInt(args[1]);
					int y = Integer.parseInt(args[2]);
					TunnelEntrance te = tunnels.get(new Position(x, y));
					if(te == null) {	// nincs ilyen tunnel entrance
						printStatus("No tunnel entrance at the given positon");
					} else {	// van ilyen tunnel entrance
						te.changeStatus();
					}
				} catch(NumberFormatException e) {
					printStatus("Invalid parameter");
				}
			}
		}

		@Override
		public String callName() {
			return "settunnel";
		}

		@Override
		public String shortDescritpion() {
			return "Az alagútszájakat lehet vele állítani";
		}

		@Override
		public String longDescritpion() {
			return "settunnel <position> Leírás: A paraméterben megadott pozícióban  " +
					"(szóközökkel elválasztott kettő darab természetes szám, amely megjelent a gettunnel által visszaadott listában is) " +
					"található alagútszáj állapotát változtatja meg, ha nem halad át éppen rajta vonat. " +
					"Ellenkező esetben hibaüzenettel tér vissza.";

		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.Game_InGame);
		}
		
	}
	
	
	/**
	 * 
	 * Leírás: szünetelteti játékot. Átlépünk Paused Game állapotba.
	 *
	 */
	private class Pause implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			playerState = PlayerState.Game_StoppedGame_PausedGame;
			engineTimer.stop();
		}

		@Override
		public String callName() {
			return "pause";
		}

		@Override
		public String shortDescritpion() {
			return "Szünetelteti játékot.";
		}

		@Override
		public String longDescritpion() {
			return "Szünetelteti játékot. Átlépünk Paused Game állapotba.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.Game_InGame);
		}
		
	}
	
	/**
	 * 
	 * Visszatérés a játékba. Átlépünk In Game állapotba.
	 *
	 */
	private class Resume implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			playerState = PlayerState.Game_InGame;
			engineTimer.start();
		}

		@Override
		public String callName() {
			return "resume";
		}

		@Override
		public String shortDescritpion() {
			return "Visszatérés a játékba";
		}

		@Override
		public String longDescritpion() {
			return "Visszatérés a játékba. Átlépünk In Game állapotba.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.Game_StoppedGame_PausedGame);
		}
		
	}
	
	
	/**
	 * 
	 * betölti a következő játékot. Átlépünk Before Start állapotba.
	 *
	 */
	private class NextGame implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			playerState = PlayerState.Game_BeforeStart;
			tablemodel.init(new File("levels\\level" + currentLevel + ".xml"));  	// betölti az aktuális játékot
			gw.SetLayout(LayoutType.Game);
			gw.invalidate();
			//azonnal indítson is
			// TODO: torolni
			//cmds.get("start").execute(args, cmds);
		}

		@Override
		public String callName() {
			return "nextgame";
		}

		@Override
		public String shortDescritpion() {
			return "Betölti a következő játékot.";
		}

		@Override
		public String longDescritpion() {
			return "Betölti a következő játékot. Átlépünk Before Start állapotba.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.Game_StoppedGame_GameWon) ||
				(ps == PlayerState.BeforeGame);
		}
		
	}
	
	
	/**
	 * 
	 *  előről kezdi az elveszített játékot. Szintén Before Start állapotba lépünk át.
	 *
	 */
	private class Retry implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			cmds.get("nextgame").execute(args, cmds); 		// kód-duplikáció elkerülése: currentLevel tartalmazza az aktuális játék számát, azt tölti be.
		}

		@Override
		public String callName() {
			return "retry";
		}

		@Override
		public String shortDescritpion() {
			return "Előről kezdi az elveszített játékot.";
		}

		@Override
		public String longDescritpion() {
			return "Előről kezdi az elveszített játékot. Before Start állapotba lépünk át.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.Game_StoppedGame_GameLost);
		}
	}
	
	
	/**
	 * loadgame <mentés neve>
	 * Leírás: Ezen parancs segítségével a paraméterben megadott
	 * névvel elmentett korábbi játékot töltjük be.
	 * Átlépünk Before Start állapotba.
	 *
	 */
	private class LoadGame implements Command {

		@Override
		public void execute(String[] args, HashMap<String, Command> cmds) {
			if(args.length < 2) {	// nincs elég paraméter
				printStatus("Invalid parameter count");
			} else {				// paraméterek száma ok
				playerState = PlayerState.Game_BeforeStart;
				tablemodel.init(new File(args[1]));
			}
		}

		@Override
		public String callName() {
			return "loadgame";
		}

		@Override
		public String shortDescritpion() {
			return "Játék betöltése";
		}

		@Override
		public String longDescritpion() {
			return "loadgame <mentés neve> Leírás: Ezen parancs segítségével " + 
					"a paraméterben megadott névvel elmentett korábbi " +
					"játékot töltjük be. Átlépünk Before Start állapotba.";
		}

		@Override
		public boolean executable(PlayerState ps) {
			return (ps == PlayerState.BeforeGame);
		}
		
	}


	public void executeCommand(String line) {
		if(line.isEmpty() == false) {	//ures sorokat nem vesszuk figyelembe
			String[] args = line.split(" ");
			String cmdName = args[0];
			Command cmd = cmds.get(cmdName);
			if (cmd != null)
			{
				if(cmd.executable(playerState)) {
					cmd.execute(args, cmds);
				} else {
					printStatus("Cannot execute this command in current state.");
				}
				if(setmessages) {	// kell irogatni, hogy milyen állapotban vagyunk, milyen parancsok vannak stb.
					cmds.get("state").execute(null, null); 	// kiírja a mostani állapotunkat
					System.out.print("Elerheto parancsok: ");
					for(Command c : cmds.values()) {
						if(c.executable(playerState)) {
							System.out.print(c.callName() + ", ");
						}
					}
					printStatus("");
				}
			}
			else
			{
				printStatus("Unknown command.");
			}

		}
		
	}


	public void keyEventHandler(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_SPACE) {
			switch(playerState) {
			case Game_BeforeStart:
				executeCommand("start");
				break;
			case Game_InGame:
				executeCommand("pause");
				break;
			case Game_StoppedGame_PausedGame:
				gw.SetLayout(LayoutType.Game);
				executeCommand("resume");
				break;
			case Game_StoppedGame_GameLost:
			case Game_StoppedGame_GameWon:
				playerState = PlayerState.BeforeGame;
				executeCommand("nextgame");
				executeCommand("start");
			default:
				break;
			}
		}
		
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			executeCommand("pause");
			gw.SetLayout(LayoutType.Menu_Pause);
		}
		
	}
	
	public void switchClicked(MouseEvent mevt)
	{
		int x =((DrawPanel) mevt.getComponent()).get_X();
		int y = ((DrawPanel) mevt.getComponent()).get_Y();
		String cmd = "setswitch " + x + " " + y;
		executeCommand(cmd);
		gw.invalidate();

	}
	
	public void tunnelClicked(MouseEvent mevt) {
		int x =((DrawPanel) mevt.getComponent()).get_X();
		int y = ((DrawPanel) mevt.getComponent()).get_Y();
		String cmd = "settunnel " + x + " " + y;
		executeCommand(cmd);
		gw.invalidate();
	}
}