package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import model.Position;
import model.TunnelEntrance;

/**
 * A TunnelEntrance greafikájáért felelős osztály. 
 * @author rapos13
 *
 */
@SuppressWarnings("serial")
public class TunnelGraph extends DrawPanel {

	private BufferedImage img;
	private TunnelEntrance te;
	
	/**
	 * ctor, a koordinátákkal
	 * @param x_
	 * @param y_
	 */
	public TunnelGraph(int x_, int y_) {
		super(x_, y_);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Az elem szomszédjai meghatározzák, hogy milyen irányú legyen a rajz. Az irányt ez a függvény számolja ki. 
	 */
	@Override
	public void calculateDirection() 
	{
		Position n1 = te.getNeighbour1().getPosition();
		Position r = te.getPosition();
		int state = te.getState();
		
		if(r.posY == n1.posY && r.posX < n1.posX)
		{
			img = Res.getInstance().getTunnel_w(state);
		}
		else if(r.posY == n1.posY && r.posX > n1.posX)
		{
			img = Res.getInstance().getTunnel_e(state);
		}
		else if(r.posY < n1.posY && r.posX == n1.posX)
		{
			img = Res.getInstance().getTunnel_s(state);
		}
		else if(r.posY > n1.posY && r.posX == n1.posX)
		{
			img = Res.getInstance().getTunnel_n(state);
		}
		
	}
	/**
	 * kirajzolja a megfelelő irányú és állapotú tunnelt 
	 */
	@Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
       calculateDirection();
        g.drawImage(img,0,0,this.getWidth(), this.getHeight(),null);
        super.drawCarriage(te, g);
    }

	public void setT(TunnelEntrance te) {
		this.te = te;
		
	}

}

