package view;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import model.CrossRail;

@SuppressWarnings("serial")
public class CrossGraph extends DrawPanel {
	
	private BufferedImage img;
	private CrossRail cr;

	public CrossGraph(int x_, int y_) {
		super(x_, y_);
		// TODO Auto-generated constructor stub
	}

	public void setCr(CrossRail cr) {
		this.cr = cr;
		
	}
	
	/**
	 * beállíta a megfelelő képet
	 */
	@Override
	public void calculateDirection() 
	{		
		
		img = Res.getInstance().getCross();
	}
	/**
	 * kirajzolja a crossrailt
	 */
	@Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
       
        g.drawImage(img,0,0,this.getWidth(), this.getHeight(),null);
        super.drawCarriage(cr, g);
    }

}


