package view;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.RenderingHints.Key;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import controller.Controller;
import model.CrossRail;
import model.EntryPoint;
import model.ExitPoint;
import model.Rail;
import model.Station;
import model.Switch;
import model.TunnelEntrance;
import view.MenuPanel.MenuType;

/**
 * A játék főablakja, itt jelennek meg az egyes menük és a pályák
 * @author rapos13
 *
 */
@SuppressWarnings("serial")
public class GameWindow extends JFrame {

	//a főablakot egy jpanel tölti ki
	private JPanel jp;
	// amiben a kissebb jpanekek vannak gridlayout-ban
	private ArrayList<DrawPanel> panels;
	
	private Controller cont;
	
	int currentLayout;
	private JPanel currentPanel;
	
	/**
	 * egy üres pályát hoz létre
	 */
	public GameWindow()
	{
		// kezedetben feltöltük a paneleket a default panellel (semmi sincs rajta, zöld színű)
		super();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		init();
	}
	
	public void setController(Controller cont)
	{
		this.cont = cont;
	}
	
	/**
	 * a ctor hívja, beállítja az egyes komponenseket és keylistenereket
	 */
	private void init()
	{
		jp = new JPanel();
		jp.setLayout(new GridLayout(20, 20));
		this.setPreferredSize(new Dimension(600, 600));
		this.setMinimumSize(new Dimension(600, 600));
		
		this.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				cont.keyEventHandler(e);
			}
		});
		
		
	}
	
	/**
	 * a mapet törli a képernyőről
	 */
	public void dropPreviousContent()
	{
		jp.removeAll();
		
		panels = new ArrayList<>();
		for(int i = 19; i>=0; --i)
		{
			for(int j = 0; j< 20; ++j)
			{
				DrawPanel dp = new DrawPanel(i, j);
				panels.add(dp);
			}
			
		}
	}
	
	public enum LayoutType { Game, Menu_Main, Menu_Pause };
	
	/**
	 * beállítja a megfelelő tartalmat a képernyőre
	 * @param layout
	 */
	public void SetLayout(LayoutType layout)
	{

		if(currentPanel != null)
			this.remove(currentPanel);
		switch(layout)
		{
		case Game: //gameview
			currentPanel = jp;

			this.add(currentPanel);
			break;
		case Menu_Main: // mainmenu
			JPanel mp = new MenuPanel(this, cont, MenuType.MT_MAIN);
			currentPanel = mp;
			this.add(currentPanel);
			break;
		case Menu_Pause:
			JPanel mp2 = new MenuPanel(this, cont, MenuType.MT_PAUSE);
			currentPanel = mp2;
			this.add(currentPanel);
			break;
		}
		
		this.pack();
		this.setVisible(true);
		this.toFront();
		this.requestFocus();
	}
	

	/**
	 * a paneleket hozzáadja a főablekhoz és megjeleníti azt, az init végén hívjuk a tablemodellből
	 */
	public void showWindow()
	{
		
		for(int i = 0; i < 400; i++)
		{
			jp.add(panels.get(i));
		}
		//this.pack();
		//this.setVisible(true);
	}

	/**
	 * a view-hoz hozzáadja a paraméterben kapott rail objektumot
	 * @param rail
	 */
	public void addRail(Rail rail) {
		int x = rail.getPosition().posX;
		int y = rail.getPosition().posY;
		RailGraph rg = new RailGraph(x, y);
		rg.setRail(rail);
		
		//hogy a panelek jó sorrendben léegyenek, kell egy kicsit a koordinátákkal dolgozni
		int yind = 19 -y;
		
		int idx = yind*20+x;
		panels.remove(idx);
		panels.add(idx, rg);	
		
	}
	
	/**
	 * a view-hoz hozzáadja a paraméterben kapott EntryPoint objektumot
	 * @param ep
	 */
	public void addEntry(EntryPoint ep) {
		int x = ep.getPosition().posX;
		int y = ep.getPosition().posY;
		EntryGraph epg = new EntryGraph(x, y);
		epg.setEp(ep);
		
		//hogy a panelek jó sorrendben léegyenek, kell egy kicsit a koordinátákkal dolgozni
		int yind = 19 -y;
		
		int idx = yind*20+x;
		panels.remove(idx);
		panels.add(idx, epg);	
		
	}
	
	/**
	 * a view-hoz hozzáadja a paraméterben kapott ExitPoint objektumot
	 * @param exp
	 */
	public void addExit(ExitPoint exp) {
		int x = exp.getPosition().posX;
		int y = exp.getPosition().posY;
		ExitGraph exg = new ExitGraph(x, y);
		exg.setExp(exp);
		
		//hogy a panelek jó sorrendben léegyenek, kell egy kicsit a koordinátákkal dolgozni
		int yind = 19 -y;
		
		int idx = yind*20+x;
		panels.remove(idx);
		panels.add(idx, exg);	
		
	}
	
	/**
	 * a view-hoz hozzáadja a paraméterben kapott TunnelEntrance objektumot
	 * @param te
	 */
	public void addTunnel(TunnelEntrance te) {
		int x = te.getPosition().posX;
		int y = te.getPosition().posY;
		TunnelGraph tg = new TunnelGraph(x, y);
		tg.setT(te);
		
		//a tunnelre kattintva meghívjuk a controller megfelelő függvényét
		tg.addMouseListener(new MouseAdapter() 
		{
			public void mousePressed(MouseEvent evt) 
			{
				cont.tunnelClicked(evt);
			}
			
		}
	);	
		
		//hogy a panelek jó sorrendben léegyenek, kell egy kicsit a koordinátákkal dolgozni
		int yind = 19 -y;
		
		int idx = yind*20+x;
		panels.remove(idx);
		panels.add(idx, tg);	
		
	}
	
	/**
	 * a view-hoz hozzáadja a paraméterben kapott Station objektumot
	 * @param st
	 */
	public void addStation(Station st) {
		int x = st.getPosition().posX;
		int y = st.getPosition().posY;
		StationGraph stg = new StationGraph(x, y);
		stg.setSt(st);
		
		//hogy a panelek jó sorrendben léegyenek, kell egy kicsit a koordinátákkal dolgozni
		int yind = 19 -y;
		
		int idx = yind*20+x;
		panels.remove(idx);
		panels.add(idx, stg);			
	}
	
	/**
	 * a view-hoz hozzáadja a paraméterben kapott CrossRail objektumot
	 * @param cr
	 */
	public void addCross(CrossRail cr) {
		int x = cr.getPosition().posX;
		int y = cr.getPosition().posY;
		CrossGraph crg = new CrossGraph(x, y);
		crg.setCr(cr);
		
		//hogy a panelek jó sorrendben léegyenek, kell egy kicsit a koordinátákkal dolgozni
		int yind = 19 -y;
		
		int idx = yind*20+x;
		panels.remove(idx);
		panels.add(idx, crg);			
	}
	
	/**
	 * a view-hoz hozzáadja a paraméterben kapott Switch objektumot
	 * @param sw
	 */
	public void addSwitch(Switch sw)
	{
		int x = sw.getPosition().posX;
		int y = sw.getPosition().posY;
		SwitchGraph rg = new SwitchGraph(x, y);
		rg.setSwitch(sw);
		// actionlistener adapter a kattintás figyelésére
		rg.addMouseListener(new MouseAdapter() 
							{
								public void mousePressed(MouseEvent evt) 
								{
									cont.switchClicked(evt);
								}
							}
						);
		
		//hogy a panelek jó sorrendben léegyenek, kell egy kicsit a koordinátákkal dolgozni
		int yind = 19 -y;
		
		int idx = yind*20+x;
		panels.remove(idx);
		panels.add(idx, rg);
	}
	
	
	/**
	 * az elemeknek több iránya is van, ezeket ki kell számítani a szomszédok 
	 * koordinátája alapján, de ezt csak az init végén tudjuk megtenni, 
	 * amikor már mindenkinek a szomszédja be van állítva
	 *tehát ezt a függvényt az init végén kell hívni
	*/
	public void initFinished()
	{
		for (DrawPanel panel : panels) {
			panel.calculateDirection();
		}
		for(int i = 0; i < 400; i++)
		{
			jp.add(panels.get(i));
		}
		invalidate();
	}

	/**
	 * megjelenít egy OKCancel ablakot a paraméterben kapott stringgel
	 * @param string
	 * @return
	 */
	public boolean showOKCancelMessageBox(String string) {
		int result = JOptionPane.showConfirmDialog(this, string,
		        "Informacio", JOptionPane.OK_CANCEL_OPTION);
		return result == JOptionPane.OK_OPTION;
		
	}
}
