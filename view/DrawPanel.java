package view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import model.Carriage;
import model.Rail;

@SuppressWarnings("serial")
public class DrawPanel extends JPanel 
{
	/**
	 * a paneleknek a koordinátája
	 */
	private int x;
	public int get_X() {
		return x;
	}

	public int get_Y() {
		return y;
	}

	private int y;
	
	
	/**
	 * beállítja az építő panel méretét (30*30 pixel)
	 * @param x_
	 * @param y_
	 */
	public DrawPanel(int x_, int y_) 
	{
		super();
		setPreferredSize(new Dimension(30, 30));
		setMinimumSize(new Dimension(30, 30));
		x=x_;
		y=y_;
		
	}

	/**
	 * itt töténik meg a háttér kirajzolása
	 */
	@Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

       //g.setFont(new Font(getToolTipText(), 1, 8));
       //g.drawString(Integer.toString(y) + "-" + Integer.toString(x), 10, 15);
       this.setBackground(new Color(200, 255, 200));
        
        
       
        
        //g.drawImage(img,0,0,this.getWidth(), this.getHeight(),null);
    }
	/**
	 * debughoz
	 */
	@Override
	public String toString() {
		return "DrawPanel [x=" + x + ", y=" + y + "]";
	}
	
	/**
	 * a leszármazottak ezt override-olják az elem irányának kiszámításához
	 */
	public void calculateDirection()
	{
		return;
	}
	
	/**
	 * A leszármazottak hívják. Ha az adott elemen tartózkodik carriage, akkor kirajzolja azt
	 * @param r
	 * @param g
	 */
	public void drawCarriage(Rail r, Graphics g)
	{
		if(r.getActualCarriage()!= null)
        {
        	Graphics2D g2d = (Graphics2D) g;        	
        	
        	model.Color c = r.getActualCarriage().getColor();
        	if(c!=null)
        	g2d.setColor(c.toAwtColor());
        	g2d.setStroke(new BasicStroke(5));
        	int N = 5;
        	if(!r.getActualCarriage().isEmpty())
        	{
        		g2d.fillRect(N, N, this.getWidth()-N-N, this.getHeight()-N-N);
        	}
        	else
        	{
        		
        		g2d.drawRect(N, N, this.getWidth()-N-N, this.getHeight()-N-N);
        	}
            
	        
        }
	}
	
}
