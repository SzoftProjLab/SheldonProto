package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import model.Position;
import model.Switch;

public class SwitchGraph extends DrawPanel {

	private Switch s;
	BufferedImage img;
	
	public SwitchGraph(int x_, int y_) {
		super(x_, y_);
	}
	
	public void setSwitch(Switch s)
	{
		this.s = s;
	}
	
	
	public void calcDir(Position n1, Position n2)
	{
		Position r = s.getPosition();
		
		
		
		if(n1.posX == n2.posX)
		{
			img = Res.getInstance().getRail_ns();
		}
		else if(n1.posY == n2.posY)
		{
			img = Res.getInstance().getRail_we();
		}
		else if(r.posX > n1.posX && r.posY < n2.posY || r.posX > n2.posX && r.posY < n1.posY)
		{
			img = Res.getInstance().getRail_wn();
		}
		else if(r.posY > n1.posY && r.posX < n2.posX || r.posY > n2.posY && r.posX < n1.posX)
		{
			img = Res.getInstance().getRail_se();
		}
		else if(r.posY > n1.posY && r.posX > n2.posX || r.posY > n2.posY && r.posX > n1.posX)
		{
			img = Res.getInstance().getRail_ws();
		}
		else if(r.posX < n1.posX && r.posY < n2.posY || r.posX < n2.posX && r.posY < n1.posY)
		{
			img = Res.getInstance().getRail_ne();
		}
		
		
		
	}
	
	/**
	 * Az elem szomszédjai meghatározzák, hogy milyen irányú legyen a rajz. Az irányt ez a függvény számolja ki. 
	 */
	@Override
	public void calculateDirection() 
	{
		Position n1 = s.getNeighbour1().getPosition();
		Position n2 = s.getNeighbour2().getPosition();
		Position n3 = s.getNeighbour3().getPosition();
		
		switch(s.getState())
		{
		case 0: // AB
			
			calcDir(n1, n2);
			
			break;
		case 1: // BC = n1 -> n2 ; n2-> n3
			
			calcDir(n2, n3);
			
			break;
		case 2: // CA = n1 -> n3 ; n2 -> n1
			
			calcDir(n3, n1);
			
			break;
		}
				
		
		
	}
	/**
	 * kirajzolja a switchet és beállítja a speciális háttérszint hozzá
	 */
	@Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
       
        this.setBackground(new Color(0, 255, 0));
        calculateDirection();
        g.drawImage(img,0,0,this.getWidth(), this.getHeight(),null);
        super.drawCarriage(s, g);
    }


}
