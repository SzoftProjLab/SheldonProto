package view;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import model.ExitPoint;
import model.Position;

@SuppressWarnings("serial")
public class ExitGraph extends DrawPanel {
	
	private BufferedImage img;
	private ExitPoint exp;

	public ExitGraph(int x_, int y_) {
		super(x_, y_);
		// TODO Auto-generated constructor stub
	}

	public void setExp(ExitPoint exp) {
		this.exp = exp;
		
	}
	/**
	 * Az elem szomszédjai meghatározzák, hogy milyen irányú legyen a rajz. Az irányt ez a függvény számolja ki. 
	 */
	@Override
	public void calculateDirection() 
	{		
		Position n1 = exp.getNeighbour1().getPosition();
		Position r = exp.getPosition();
		
		if(r.posY == n1.posY && r.posX < n1.posX)
		{
			img = Res.getInstance().getExit_w();
		}
		else if(r.posY == n1.posY && r.posX > n1.posX)
		{
			img = Res.getInstance().getExit_e();
		}
		else if(r.posY < n1.posY && r.posX == n1.posX)
		{
			img = Res.getInstance().getExit_s();
		}
		else if(r.posY > n1.posY && r.posX == n1.posX)
		{
			img = Res.getInstance().getExit_n();
		}
	}
	/**
	 * kirajzolja a megfelelő irányú railt és ha van rajta vonat akkor azt is
	 */
	@Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
       
        g.drawImage(img,0,0,this.getWidth(), this.getHeight(),null);
        super.drawCarriage(exp, g);
    }

	

}

