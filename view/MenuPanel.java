package view;

import java.awt.Button;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import controller.Controller;
import view.GameWindow.LayoutType;

@SuppressWarnings("serial")
public class MenuPanel extends JPanel {

	Controller cont;
	
	public enum MenuType {MT_MAIN, MT_PAUSE};
	
	public MenuPanel(GameWindow gw, Controller cont, MenuType mt)
	{
		super();
		this.cont = cont;
		GridLayout gl = new GridLayout(2,1);
		this.setLayout(gl);
		
		
		if(mt.equals(MenuType.MT_PAUSE))
		{
			Button resume = new Button("Resume");
			resume.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					gw.SetLayout(LayoutType.Game);
					cont.executeCommand("resume");
					
				} });
			Button exit = new Button("Exit");
			exit.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					cont.executeCommand("exit");
					
				} });
			
			this.add(resume);
			this.add(exit);
		}
		else
		{
			Button newgame = new Button("New Game");
			newgame.addActionListener(new ActionListener(){
	
				@Override
				public void actionPerformed(ActionEvent e) {
					cont.executeCommand("newgame");
				} });
			Button exit = new Button("Exit");
			exit.addActionListener(new ActionListener() {
	
				@Override
				public void actionPerformed(ActionEvent e) {
					cont.executeCommand("exit");
				} });
			
			this.add(newgame);
			this.add(exit);
		}
		
	}
	
}
