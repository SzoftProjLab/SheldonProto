package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import model.Position;
import model.Rail;

@SuppressWarnings("serial")
public class RailGraph extends DrawPanel {

	Rail rail;
	BufferedImage img;
	
	public RailGraph(int x_, int y_) {
		super(x_, y_);
		
	}
	
	public void setRail(Rail r)
	{
		rail = r;
		
		
	}
	
	/**
	 * a rail szomszédjai alapján meghatározza, hogy milyen irányú rail-t kell kirajzolni
	 */
	@Override
	public void calculateDirection() 
	{
		Position n1 = rail.getNeighbour1().getPosition();
		Position n2 = rail.getNeighbour2().getPosition();
		Position r = rail.getPosition();
		
		
		
		if(n1.posX == n2.posX)
		{
			img = Res.getInstance().getRail_ns();
		}
		else if(n1.posY == n2.posY)
		{
			img = Res.getInstance().getRail_we();
		}
		else if(r.posX > n1.posX && r.posY < n2.posY || r.posX > n2.posX && r.posY < n1.posY)
		{
			img = Res.getInstance().getRail_wn();
		}
		else if(r.posY > n1.posY && r.posX < n2.posX || r.posY > n2.posY && r.posX < n1.posX)
		{
			img = Res.getInstance().getRail_se();
		}
		else if(r.posY > n1.posY && r.posX > n2.posX || r.posY > n2.posY && r.posX > n1.posX)
		{
			img = Res.getInstance().getRail_ws();
		}
		else if(r.posX < n1.posX && r.posY < n2.posY || r.posX < n2.posX && r.posY < n1.posY)
		{
			img = Res.getInstance().getRail_ne();
		}
				
		
		
	}
	/**
	 * kirajzolja a megfelelő irányú railt és ha van rajta vonat akkor azt is
	 */
	@Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
       
        g.drawImage(img,0,0,this.getWidth(), this.getHeight(),null);
        super.drawCarriage(rail, g);
    }

}
