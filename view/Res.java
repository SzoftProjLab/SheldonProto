package view;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

/**
 * singleton osztály az erőforrások tárolására és betöltésére
 * @author rapos13
 *
 */
public class Res {
	private BufferedImage cross;
 
	private BufferedImage entry_e;
	private BufferedImage entry_n;
	private BufferedImage entry_s;
	private BufferedImage entry_w;
	
	private BufferedImage exit_e;
	private BufferedImage exit_n;
	private BufferedImage exit_s;
	private BufferedImage exit_w;
	 
	private BufferedImage rail_ne;
	private BufferedImage rail_ns;
	private BufferedImage rail_se;
	private BufferedImage rail_we;
	private BufferedImage rail_ws;
	private BufferedImage rail_wn;
	
	private BufferedImage tunnel_p_e;
	private BufferedImage tunnel_p_n;
	private BufferedImage tunnel_p_s;
	private BufferedImage tunnel_p_w;
	
	private BufferedImage tunnel_c_e;
	private BufferedImage tunnel_c_n;
	private BufferedImage tunnel_c_s;
	private BufferedImage tunnel_c_w;
	
	private BufferedImage tunnel_o_e;
	private BufferedImage tunnel_o_n;
	private BufferedImage tunnel_o_s;
	private BufferedImage tunnel_o_w;
	
	private String resFolder = "res/";
	
	/**
	 * a szingleton mintának megfelelően a ctor private. Itt töltjük be a fájlokból a képeket
	 */
	private Res()
	{
		try{
        	rail_ne = ImageIO.read(new File(resFolder + "rail_ne.png"));
        	rail_ns = ImageIO.read(new File(resFolder + "rail_ns.png"));
        	rail_se = ImageIO.read(new File(resFolder + "rail_se.png"));
        	rail_we = ImageIO.read(new File(resFolder + "rail_we.png"));
        	rail_ws = ImageIO.read(new File(resFolder + "rail_ws.png"));
        	rail_wn = ImageIO.read(new File(resFolder + "rail_wn.png"));
        	
        	entry_e = ImageIO.read(new File(resFolder + "entry_e.png"));
        	entry_n = ImageIO.read(new File(resFolder + "entry_n.png"));
        	entry_s = ImageIO.read(new File(resFolder + "entry_s.png"));
        	entry_w = ImageIO.read(new File(resFolder + "entry_w.png"));
        	
        	exit_e = ImageIO.read(new File(resFolder + "exit_e.png"));
        	exit_n = ImageIO.read(new File(resFolder + "exit_n.png"));
        	exit_s = ImageIO.read(new File(resFolder + "exit_s.png"));
        	exit_w = ImageIO.read(new File(resFolder + "exit_w.png"));
        	
        	tunnel_p_e = ImageIO.read(new File(resFolder + "tunnel_p_e.png"));
        	tunnel_p_n = ImageIO.read(new File(resFolder + "tunnel_p_n.png"));
        	tunnel_p_s = ImageIO.read(new File(resFolder + "tunnel_p_s.png"));
        	tunnel_p_w = ImageIO.read(new File(resFolder + "tunnel_p_w.png"));
        	
        	tunnel_c_e = ImageIO.read(new File(resFolder + "tunnel_c_e.png"));
        	tunnel_c_n = ImageIO.read(new File(resFolder + "tunnel_c_n.png"));
        	tunnel_c_s = ImageIO.read(new File(resFolder + "tunnel_c_s.png"));
        	tunnel_c_w = ImageIO.read(new File(resFolder + "tunnel_c_w.png"));
        	
        	tunnel_o_e = ImageIO.read(new File(resFolder + "tunnel_o_e.png"));
        	tunnel_o_n = ImageIO.read(new File(resFolder + "tunnel_o_n.png"));
        	tunnel_o_s = ImageIO.read(new File(resFolder + "tunnel_o_s.png"));
        	tunnel_o_w = ImageIO.read(new File(resFolder + "tunnel_o_w.png"));
        }
        catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static Res instance;
	
	static{
		instance = new Res();
	}

	public BufferedImage getCross() {
		return cross;
	}

	public BufferedImage getEntry_e() {
		return entry_e;
	}

	public BufferedImage getEntry_n() {
		return entry_n;
	}

	public BufferedImage getEntry_s() {
		return entry_s;
	}

	public BufferedImage getEntry_w() {
		return entry_w;
	}

	public BufferedImage getExit_e() {
		return exit_e;
	}

	public BufferedImage getExit_n() {
		return exit_n;
	}

	public BufferedImage getExit_s() {
		return exit_s;
	}

	public BufferedImage getExit_w() {
		return exit_w;
	}

	public BufferedImage getRail_ne() {
		return rail_ne;
	}

	public BufferedImage getRail_ns() {
		return rail_ns;
	}

	public BufferedImage getRail_se() {
		return rail_se;
	}

	public BufferedImage getRail_we() {
		return rail_we;
	}

	public BufferedImage getRail_ws() {
		return rail_ws;
	}

	public BufferedImage getRail_wn() {
		return rail_wn;
	}

	public static Res getInstance() {
		return instance;
	}

	/**
	 * 
	 * @param state
	 * @return
	 */
	public BufferedImage getTunnel_w(int state) {
		
		switch (state) {
		case 0:
			return tunnel_p_w;
		case 1:
			return tunnel_c_w;
		case 2:
			return tunnel_o_w;
		default:
			break;
		}
		return null; 
	}
	/**
	 * 
	 * @param state
	 * @return
	 */
	public BufferedImage getTunnel_e(int state) {
			
			switch (state) {
			case 0:
				return tunnel_p_e;
			case 1:             
				return tunnel_c_e;
			case 2:             
				return tunnel_o_e;
			default:
				break;
			}
			return null; 
		}
	/**
	 * 
	 * @param state
	 * @return
	 */
	public BufferedImage getTunnel_s(int state) {
	
		switch (state) {
		case 0:
			return tunnel_p_s;
		case 1:
			return tunnel_c_s;
		case 2:
			return tunnel_o_s;
		default:
			break;
		}
		return null; 
	}
	/**
	 * 
	 * @param state
	 * @return
	 */
	public BufferedImage getTunnel_n(int state) {
	
		switch (state) {
		case 0:
			return tunnel_p_n;
		case 1:
			return tunnel_c_n;
		case 2:
			return tunnel_o_n;
		default:
			break;
		}
		return null; 
	}

}
