package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import model.Position;
import model.Station;

@SuppressWarnings("serial")
public class StationGraph extends DrawPanel {
	private BufferedImage img;
	private Station st;

	public StationGraph(int x_, int y_) {
		super(x_, y_);
		// TODO Auto-generated constructor stub
	}

	public void setSt(Station st) {
		this.st = st;
		
	}
	/**
	 * Az elem szomszédjai meghatározzák, hogy milyen irányú legyen a rajz. Az irányt ez a függvény számolja ki. 
	 */
	@Override
	public void calculateDirection() 
	{		
		Position n1 = st.getNeighbour1().getPosition();
		Position n2 = st.getNeighbour2().getPosition();
		Position r = st.getPosition();
		
		if(n1.posX == n2.posX)
		{
			img = Res.getInstance().getRail_ns();
		}
		else if(n1.posY == n2.posY)
		{
			img = Res.getInstance().getRail_we();
		}
		else if(r.posX > n1.posX && r.posY < n2.posY || r.posX > n2.posX && r.posY < n1.posY)
		{
			img = Res.getInstance().getRail_wn();
		}
		else if(r.posY > n1.posY && r.posX < n2.posX || r.posY > n2.posY && r.posX < n1.posX)
		{
			img = Res.getInstance().getRail_se();
		}
		else if(r.posY > n1.posY && r.posX > n2.posX || r.posY > n2.posY && r.posX > n1.posX)
		{
			img = Res.getInstance().getRail_ws();
		}
		else if(r.posX < n1.posX && r.posY < n2.posY || r.posX < n2.posX && r.posY < n1.posY)
		{
			img = Res.getInstance().getRail_ne();
		}
	}
	/**
	 * kirajzolja a megfelelő irányú station-t és a várakozó utasokat jelképező négyzeteket
	 */
	@Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.setBackground(st.getColor().toAwtColor());
        g.drawImage(img,0,0,this.getWidth(), this.getHeight(),null);
        
        super.drawCarriage(st, g);
        
        ArrayList<model.Color> passangers = st.getPassengers();
        Graphics2D g2d = (Graphics2D) g;
        
			for(int i = 0; i< passangers.size(); ++i)
			{
				g2d.setColor(passangers.get(i).toAwtColor());
				g2d.fillRect(3, i*5, 5, 5);
			}
		}
    
	
	

}

