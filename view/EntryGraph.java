package view;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import model.EntryPoint;
import model.Position;

public class EntryGraph extends DrawPanel {
	
	private BufferedImage img;
	private EntryPoint ep;
	
	
	

	public void setEp(EntryPoint ep) {
		this.ep = ep;
	}

	public EntryGraph(int x_, int y_) {
		super(x_, y_);
		// TODO Auto-generated constructor stub
	}
	/**
	 * Az elem szomszédjai meghatározzák, hogy milyen irányú legyen a rajz. Az irányt ez a függvény számolja ki. 
	 */
	@Override
	public void calculateDirection() 
	{		
		Position n1 = ep.getNeighbour1().getPosition();
		Position r = ep.getPosition();
		
		if(r.posY == n1.posY && r.posX < n1.posX)
		{
			img = Res.getInstance().getEntry_w();
		}
		else if(r.posY == n1.posY && r.posX > n1.posX)
		{
			img = Res.getInstance().getEntry_e();
		}
		else if(r.posY < n1.posY && r.posX == n1.posX)
		{
			img = Res.getInstance().getEntry_s();
		}
		else if(r.posY > n1.posY && r.posX == n1.posX)
		{
			img = Res.getInstance().getEntry_n();
		}
	}
	/**
	 * kirajzolja a megfelelő irányú railt és ha van rajta vonat akkor azt is
	 */
	@Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
       
        g.drawImage(img,0,0,this.getWidth(), this.getHeight(),null);
        super.drawCarriage(ep, g);
    }

}
