package model;

import java.util.*;

import controller.Controller;

/**
 * Állapotai vannak: nincs megépítve, 1 darab van megépítve (zárt) és kettő van megépítve (nyitott). Kettőnél több megépített nem lehet. Ezt egy statikus tagváltozóval ellenőrizzük. Megszüntetni csak akkor lehet, ha nincs vonat (Carriage objektum) az alagútban.
 * 
 * Két megépített TunnelEntrance között Rail objektumok vannak, számuk rögzített. (minden alagút ugyan olyan hosszú a pályán, az alagútszályak pozíciójától függetlenül. ) Őket az osztály statikus inicializálójában hozzuk létre.  Minden TunnelEntrance párhoz ugyan az az alagútszakasz tartozik. Az alagútszakasz két végpontját az osztály statikus tagváltozójában tároljuk. Ezeket végpontokat mindig az aktuálisan nyitott TunnelEntrance - ekhoz hozzá kell rendelni. Ezt a feladatot a privát build() és destroy() tagfügvények végzik. Nem probléma, hogy csak egy alagútszakasz van, mivel minden időpillatban maximum kettő alagútszáj lehet.
 * 
 * TunnelEntrance megszüntetésekor ellenőrizni kell, hogy van e vonat az alagútban. Ha van, akkor nem lehet megszüntetni azt. Tehát számon kell tartani, hogy van - e Carriage objektum az alagút belsejében, ezt egy statikus számlálóval valósítjuk meg. Itt fontos szerepet kap az, hogy a vonat milyen irányból érkezik az alagútszájhoz.
 * Ha a vonat valamelyik tunnelEnds - ről érkezik, akkor csökkentjük a számláló értékét, ha nem ezek valalmelyikéről érkezik, akkor növeljük.
 */
public class TunnelEntrance extends Rail
{	
	private static int builtEntrances = 0;
	
	public static void setBuiltEntrances(int builtEntrances) {
		TunnelEntrance.builtEntrances = builtEntrances;
	}
	/**
	 * A két alagútszáj között futó sinszakasz hossz előre rögzített. Ez egy olyan Rail szakasz, aminek kezdetben egyik vége sem kapcsolódik sehová sem. Mikor megépül egy alagútszáj akkor az egyik (szabad) végpontot hozzá kell állítani az új alagútszáj neighbour2 - jébe.
	 */
	private static ArrayList<Rail> tunnelEnds = new ArrayList<Rail>();
	/**
	 * Azt számlálja, hogy hány darab Carriage objektum van az alagútban.
	 */
	private static int carriageCounter;

	/**
	 * Ha nics megépítve az alagút száj megépíti azt (ha már nics kettő a pályán), ha meg van építve, akkor megszünteti azt. (ha nincs az alagútban vonat. )
	 */
	// belső működéshez tartozó privát változók
	private static TunnelEntrance firstOpen = null; 
	private static TunnelEntrance secondOpen = null;
	
	private State [] states = {new noEntrance(),new closedEntrance(),new openEntrance()};
	private int currentState = 0;
	
	public int getState()
	{
		return currentState;
	}
	
	//eddig
	
	@Override
	public void registerIn(Carriage carriage, Rail from) throws AccidentException{
		super.registerIn(carriage, from);
		states[currentState].carriageArrived();
		if(from == neighbour1)
			carriageCounter++;
		else{
			carriageCounter--;
		}
		
	}
	
	public void changeStatus()
	{
		int oldState = currentState;
		currentState = states[currentState].changeStatus();
		if(oldState != currentState)
			switch(currentState){	
			 	case 0:
			 		Controller.print("noEntrance");
			 		break;
		 		case 1:
			 		Controller.print("closedEntrance");
			 		break;
		 		case 2:
			 		Controller.print("openEntrance");
			 		break;
			}
	}

	/**
	 * Az alagút építését végzi el. Ha már meg van építve akkor nem tesz semmit
	 */
	private void build()
	{
		if(currentState == 0){
			changeStatus();
		}
	}

	/**
	 * Az alagútszáj megszüntetése. Ha már meg van szüntetve nem tesz semmit
	 */
	private void destroy()
	{
		if(currentState !=0 )
			changeStatus();
	}

	/**
	 * Ellenőrzi, hogy van - vonat z alagútban.
	 * @return True: carriageCounter > 0
	 */
	private boolean isCarriageInTheTunnel()
	{
		return carriageCounter > 0;
	}

	/**
	 * Ezzel a függvénnyle lehet beállítani a Rail objektum szomszédjainak referenciáját.
	 * Itt csak egy referenciát kell beállítani, méghozzá a aneighbour1 - be. A másik referenciát null-ra inicializáljuk.
	 * Nyitott bejáratra ne hívjuk meg mert elkúr mindent
	 * @param rail1 Az egyik szomszéd.
	 * @param rail2 A másik szomszéd.
	 */
	public void setNeighbourRails(Rail rail1, Rail rail2)
	{
		super.setNeighbourRails(rail1, null);
	}

	/**
	 * Ellenőrzi, hogy van-e Carriage a sínen.True: van, False: nincs.
	 * TunnelEntrance esetében akkor is True - val térvissza, ha a TunnelEntrance állapota az, hogy nincs megépítve, vagy csak egy van megépítve  a pályán.
	 * @return True: van False: nincs
	 */
	@Override
	public boolean isObstacleOnRail()
	{
		return super.isObstacleOnRail() || currentState != 2;
	}
	/**
	 * Ha ez az első entrance, létrehozza a belső raileket 6 darabot alapértelemzetten.
	 */
	public TunnelEntrance()
	{
		super();
		carriageCounter = 0;
		int tunnellength = 5;
		Rail previous = null;
		if(tunnelEnds.size() != tunnellength){
			for(int i = 0; i<tunnellength;i++){
				Rail newrail = new Rail();
				newrail.setPosition(new Position(-1,i));
				if(i >0)
					tunnelEnds.get(i-1).setNeighbourRails(tunnelEnds.get(i-1).neighbour1, newrail);
				newrail.setNeighbourRails(previous, null);
				tunnelEnds.add(newrail);
				previous = newrail;
			}
		}
	}

	/**
	 * 
	 * @param set
	 */
	
	
	
	public void setBuilt_t(boolean set)
	{
		if(set)
			build();
		else
			destroy();
	}
	// belső működéshez tartozó privát metódusok és osztályok
	
	private void signalSecondToggled(){
		currentState =  states[currentState].secondToggled();
	}
	
	private abstract class State{
		public int changeStatus(){ return 0;};
		public int secondToggled(){return 0;};
		public void carriageArrived()throws AccidentException{};
		
	}
	private class noEntrance extends State{
		@Override
		public int changeStatus(){
			switch(builtEntrances){
				case 0:
					firstOpen = TunnelEntrance.this;
					Rail newneighbour1 = tunnelEnds.get(0);
					newneighbour1.setNeighbourRails(TunnelEntrance.this, newneighbour1.neighbour2);
					TunnelEntrance.this.neighbour2 = newneighbour1;
					builtEntrances++;
					return 1;
				case 1:
					secondOpen = TunnelEntrance.this;
					Rail newneighbour2 = tunnelEnds.get(tunnelEnds.size()-1);
					newneighbour2.setNeighbourRails(newneighbour2.neighbour1, TunnelEntrance.this);
					TunnelEntrance.this.neighbour2 = newneighbour2;
					builtEntrances++;
					firstOpen.signalSecondToggled();
					return 2;
				case 2:
					Controller.print("Max number of open tunnel entrances is 2, cannot open");
					return 0;
				default:
					return 0;
			}
			
		}
		@Override
		public void carriageArrived() throws AccidentException{
			throw new AccidentException();
		}
		
		
	}
	private class closedEntrance extends State{
		@Override
		public int changeStatus(){
			builtEntrances--;
			return 0;
		}
		@Override
		public int secondToggled(){
			return 2;
		}
		@Override
		public void carriageArrived() throws AccidentException{
			throw new AccidentException();
		}
		
		
	}
	private class openEntrance extends State{
		@Override
		public int changeStatus(){
			if(isCarriageInTheTunnel() == false){
				if(firstOpen == TunnelEntrance.this){
					firstOpen = secondOpen;
					Rail newneighbour1 = tunnelEnds.get(0);
					newneighbour1.setNeighbourRails(TunnelEntrance.this, newneighbour1.neighbour2);
					TunnelEntrance.this.neighbour2 = newneighbour1;
				}
				firstOpen.signalSecondToggled();
				builtEntrances--;
				return 0;
			}else{
				Controller.print("Tunnel is currently occupied, cannot destroy");
				return 2;
			}
		}
		@Override
		public int secondToggled(){
			return 1;
		}
		@Override
		public void carriageArrived() throws AccidentException{
			
		}
		
	}
}