package model;

import controller.Controller;

/**
 * @author Juhos Attila
 * @date 2017.04.15
 * 
 * A Carrige-ben utasok utaznak. 
 * Van neki színe. Ha Station-höz ér és a színek egyeznek és ő az első nem üres kocsi akkor üressé válik. 
 * A kocsikat a moveFurther() függvény meghívásával lehet előre mozgatni. 
 * Ha a Station-ön utasok is szállnak fel, akkor a mögötte levő kocsinak szól, hogy már nem üres, 
 * és, hogy róla már ne szálljanak le utasok. Ezt az üzenetet a mögötte levő kocsi továbbadja.
 */
public class Carriage
{
	/**
	 * Várakozási Idő. Mértékegység: [darab timeOut]
	 * Ha a vonathoz tartozó Engine startDelaye n, akkor az első kocsi -é n+1 és így tovább. Ezt a változót az addCarriage be tudja állítani jól.
	 */
	protected int startDelay;
	/**
	 * Adott Rail-ből mikor továbbmegy, megjegyzi, hogy honnan megy tovább.
	 */
	private Rail lastRail;
	/**
	 * A carriage aktuális poziciója.
	 */
	private Rail actualRail;
	/**
	 * A carriage után hátrafelé a következő carriage.
	 */
	private Carriage nextCarriage = null;
	private Color color;
	public Color getColor() {
		return color;
	}

	public boolean isEmpty() {
		return empty;
	}

	/**
	 * van e utas  a kocsiban
	 */
	private boolean empty;
	/**
	 * a kocsi előtt az összes többi kocsi üres
	 */
	private boolean allPrevCarriageAreEmpty;
	/**
	 * A kocsit "húzó" Engine
	 */
	protected Engine myEngine;

	/**
	 * Hozzáad egy Carriage-t a vonathoz. Az utolso Carriage nextCarriage atrributuma a hozzáadott car-ra fog mutatni. 
	 * Ezenkívül beállítja még a startDelayeket.
	 * Ezt a függvényt először az engine objetumon hívjuk, és ő hívja tovább a mögötte levő carriage - ken.
	 * @param newCar az új Carriage
	 */
	public void addCarriage(Carriage newCar)
	{
		// Vizsgáljuk, hogy következik-e utánunk vagon.
		// Ha igen, akkor annak adjuk át az új kocsit.
		if(nextCarriage == null) {
			nextCarriage = newCar;
			// Beállítjuk a megfelelő késleltetést. Eggyel több, mint nekünk.
			newCar.startDelay = this.startDelay + 1;
		} else {
			nextCarriage.addCarriage(newCar);
		}
	}

	/**
	 * A következő Rail objektumra mozgatja a Carriage-t, és ha a nextCarriage nem NULL 
	 * (nem az utolsó kocsinál vagyunk) meghívja annak is a moveFurther függvényét.
	 */
	protected void moveFurther()
	{
		try {
			if(startDelay > 0) {
				waitingForStart();
			}
			else {
				// Az actualRail akkor lehet null, ha ExitPointhoz érve mindenkit kiregszitrálunk az actualRailtől
				// ÉS az actualRail referenciát null-ra állítjuk. Lásd a Carriage.carriageFinished() függvényt.
				if(actualRail != null) {
					lastRail = actualRail;
					Rail nextRail = actualRail.registerOut();
									
					actualRail = nextRail;
					
					//pl. Engine moved from: Rail 0 3 to: Station 0 4
					Controller.print(
							this.getClass().getSimpleName() + 
							" moved from: " + 
							lastRail.getClass().getSimpleName() +
							" " +
							lastRail.positionToString() +
							" to: " +
							actualRail.getClass().getSimpleName() +
							" " +
							actualRail.positionToString()
							);
					if(actualRail != null) {
						actualRail.registerIn(this, lastRail);
						
					}
				}
			}
			if(nextCarriage != null) {
				nextCarriage.moveFurther();
			}
		} catch(AccidentException e) {
			if(nextCarriage != null) {
				nextCarriage.moveFurther();
			}
			announceAccident();
		}
	}

	/**
	 * A Station objektumok hívják. Ellenőrzi, hogy a kocsiból leszálhatnak -e az utasok. 
	 * Ha igen, akkor a következő kocsinak megmondja, hogy előtte már üres minden kocsi.
	 * Ha az utolsó kocsi ürült ki, akkor a kocsihoz tartozó Engine értesíti a hozzá tartozó EntryPointot.
	 * Ha még ezek után is folytatódik a játék, akkor a függvény elkéri a Station megfelelő színű utasait a getIn() függvény megvizsgálásával. 
	 * Ha új utasok szálltak fel, akkor jelezzük a mögöttünk levő vagonnak, hogy már nem minden vagon üres előtte. 
	 * Ehhez a setAllPrecCarriageIsEmpty(bloolean b) függvényt használjuk.
	 * @param stationColor
	 */
	public void carriageOnStation(Station station, Color stationColor)
	{
		if(this.allPrevCarriageAreEmpty == true && empty == true)
		{
			if(this.nextCarriage!=null) this.nextCarriage.setAllPrevCarriageIsEmpty(true);
		}
		if(empty == false && allPrevCarriageAreEmpty == true && color == stationColor) {
			getOff();
		}
		
		if(empty == true) {
			
			boolean newPassenger = station.getIn(color);
			
			if(newPassenger) {
				empty = false;
				Controller.print("Carriage got in " + color);
				if(nextCarriage != null) {
					nextCarriage.setAllPrevCarriageIsEmpty(false);
				}
				
			}
		}
	}

	/**
	 * A kocsi kiürítése ( empty attributum True értéket kap).
	 */
	protected void getOff()
	{
		// Mi is kiürülünk.
		empty = true;
		Controller.print("Carriage got off " + color);
		// De a következő vagonnak is zöld lámpa, hogy az utasai leszálljanak.
		if(nextCarriage != null) {
			nextCarriage.setAllPrevCarriageIsEmpty(true);
		} else {
			myEngine.trainBecomeEmpty();
		}
	}

	/**
	 * Ellenörzi hogy a vonat üres -e. Ha nem üres ,akkor "baleset történt".
	 * Ha üres a vonat, akkor kiveszi a vonatot, mert többé már nem kell vezérelni, és a vonat összes kocsija eltűnik a pályáról. 
	 * (az összes kocsi meghívja a registerOut() függvényt mint a moveFurther - nél, csak nem registerIn() - el.) 
	 * Ezt a carriageFinish() függvénnyel teszi.
	 * @throws AccidentException 
	 */
	public void onExitPoint() throws AccidentException
	{
		if(isTrainEmpty() == false) {
			throw new AccidentException();
		} else {
			removeTimer();
			carriageFinished();
			myEngine.trainBecomeEmpty();
		}
		
	}

	/**
	 * Ha a paraméter true, akkor a beállítja a flaget truera, vagyis előtte mindenki üres.
	 * Ha a paraméter false, akkor beállítja a flaget falsera és a nextCarriagen is meghívja ezt.
	 * @param b
	 */
	protected void setAllPrevCarriageIsEmpty(boolean b)
	{
		allPrevCarriageAreEmpty = b;
		
		if(b == false && nextCarriage != null) {
			nextCarriage.setAllPrevCarriageIsEmpty(false);
		}
	}

	/**
	 * Rekurzív hívás egy Carriage -n. 
	 * Hátrafelé rekurzívan meghívjuk minden carriage-ra.
	 * Ha az adott carriage üres, akkor tovább hívunk hárta felé, ha nem akkor visszatérünk false-al. 
	 * Akkor true, ha azutolsó kocsi is üres. Az ExitPointnál hívódik.
	 */
	protected boolean isTrainEmpty()
	{
		if(!empty)
			return false;
		else if(nextCarriage != null)
			return nextCarriage.isTrainEmpty();
		else return true;
	}

	/**
	 * Akkor hívódik meg, amikor baleset történik.
	 */
	protected void announceAccident()
	{
		
		myEngine.announceAccident();
	}

	/**
	 * A vonatot kiveszi az engineTimer vezérlési listájából.
	 * Ehhez meghívja az Engine hasonló függvényét.
	 */
	protected void removeTimer()
	{
		myEngine.removeTimer();
	}

	/**
	 * Meghívja a registerOut() függvényt és szól a mögötte levőnek, hogy tegyen ő is így. Ezzel a vonat eltűnik a pályáról.
	 */
	protected void carriageFinished()
	{
		actualRail.registerOut();
		actualRail = null;
		
		if(nextCarriage != null)
			nextCarriage.carriageFinished();
	}

	/**
	 * Konstruktor. Konstruktor. Az adott színnel létrehozza a Carriage-t és beállítja, hogy van-e rajta utas. 
	 * Paraméterként megkapja a kocsihoz tartozó Engine-t és az aktuális sínelemet is.
	 * Hozzaadja magát a kapott Engine-hez.
	 * @param c a kocsi színe
	 * @param empty true: üres kocsi
	 * @param engine
	 * @param actualRail
	 */
	public Carriage(Color c, boolean empty, Engine engine, Rail actualRail)
	{
		// Beállítjuk a megfelelő paramtéreket.
		color = c;
		this.empty = empty;
		myEngine = engine;
		this.actualRail = actualRail;
		
		// Mivel az EntryPoint hívja ezt a konstruktort, ezért itt hozzaadjuk az Engine-hez magunkat. Ő aztán rekurzív hívással
		// hozzadja a vonathoz a kocsit.
		if(myEngine != null)
			myEngine.addCarriage(this);
	}

	/**
	 * A Carriage - ek az EntryPointokról késleltetve indulnak. Amíg a vonat késleltetve van ez a függvény fut minden moveFurther - nél. Futásakor a startDelay változója az Enginek eggyel csökken.
	 */
	private void waitingForStart()
	{
		startDelay -= 1;
	}

}