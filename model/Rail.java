package model;


/**
 * Általános építőelem. A többi építőelemnek az ősosztálya. Az egyik végén érkező Carriage-t továbbküldi a másik végén. Tudja, hogy van - e rajta kocsi.
 */
public class Rail
{
	/**
	 * A sínen levő aktuális Carriage objektum.
	 */
	protected Carriage actualCarriage;
	public Carriage getActualCarriage() {
		return actualCarriage;
	}

	/**
	 * A sínelem egyik szomszédja.
	 */
	protected Rail neighbour1;
	/**
	 * A sínelem másik szomszédja.
	 */
	protected Rail neighbour2;
	
	public Rail getNeighbour1() {
		return neighbour1;
	}

	public Rail getNeighbour2() {
		return neighbour2;
	}
	
	

	/**
	 * A legutóbb beregisztrált Carriage objektum erről a sínről érkezett.
	 */
	protected Rail from;
	
	/**
	 * A síndarab pozícióját tartalmazza.
	 */
	private Position position;

	/**
	 * Ezzel a függvénnyle lehet beállítani a Rail objektum szomszédjainak referenciáját.
	 * @param rail1 Az egyik szomszéd.
	 * @param rail2 A másik szomszéd.
	 */
	public void setNeighbourRails(Rail rail1, Rail rail2)
	{
		this.neighbour1 = rail1;
		this.neighbour2 = rail2;
	}

	/**
	 * Ha Carriage érkezik a Rail objektumhoz, ezzel a függvénnyel regisztrál be hozzá. Ellenőrzi, hogy a sínen van-e már más objektum és igenlő esetben kivételt dob el.Beállítja az actualCarriage és a from objektumot.
	 * @param carriage Az aktuális vonatelem, amely ráhajt a sínre.
	 * @param from Honnan jön a vonat. Azért kell tudni, mert különben nem tudná a Rail, hogy merre küldje tovább a Carriaget.
	 */
	public void registerIn(Carriage carriage, Rail from) throws AccidentException
	{
		if(isObstacleOnRail())
			throw new AccidentException();
		actualCarriage = carriage;
		this.from = from;
	}

	/**
	 * Amikor egy Carriage objektum elhagyja a RAil objektumok, akkor ezt a függvényt hívja. A Rail objektumban törli a referenciát az aktuális Carriage objektumra.
	 * @return Az következő sín az úton.
	 */
	public Rail registerOut()
	{
		Rail ret = null;
		if(from == neighbour1)
			ret = neighbour2;
		if(from == neighbour2)
			ret = neighbour1;
		
		actualCarriage = null;
		from = null;
		
		return ret;
		
	}

	/**
	 * Ellenőrzi, hogy van-e Carriage a sínen.
	 * @return True: van False: nincs.
	 */
	public boolean isObstacleOnRail()
	{
		if(actualCarriage != null)
			return true;
		return false;
	}

	/**
	 * Konstruktor.
	 */
	public Rail()
	{
		actualCarriage = null;
		from = null;
		neighbour1 = null;
		neighbour2 = null;
		position = null;
	}

	/**
	 * Beállítja a sín pozícióját.
	 * @param pos A beállítandó pozíció.
	 * @return A beállított pozíció.
	 */
	public Position setPosition(Position pos)
	{
		position = pos;
		return position;
	}

	/**
	 * Lekérdezi a sín pozícióját.
	 * @return A sín pozíciója;
	 */
	public Position getPosition()
	{
		return position;
	}
	
	protected String positionToString() {
		return getPosition().posX + " " + getPosition().posY;
	}

}