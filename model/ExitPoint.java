package model;

public class ExitPoint extends Rail
{

	/**
	 * Szól a Carriage - nek, hogy ExitPointhoz érkezett. (meghívja az onExitPoint() függvényét. )
	 * @param carriage Az aktuális vonatelem, amely ráhajt a sínre.
	 * @param from
	 * @throws AccidentException 
	 */
	public void registerIn(Carriage carriage, Rail from) throws AccidentException
	{
		super.registerIn(carriage, from);
		carriage.onExitPoint();
	}

	public ExitPoint()
	{
		super();
	}

}