package model;

import java.util.*;



import controller.Controller;
import parser.*;
import view.GameWindow;

/**
 * A játék adminisztrációjáért felel.
 */
public class TableModel
{

	GameWindow gw;
	/**
	 * Az EntryPointokat egy listában tárolja. A pályáról való kommunikáció az EntryPointokon keresztül történik.
	 */
	private ArrayList<EntryPoint> entryPoints = new ArrayList<>();
	/**
	 * A pályán levő összes vonathoz pontosan egy darab engineTimer tartozik, ezt az egy példányt tároljuk ebben az attributumban.
	 */
	private EngineTimer engineTimer;
	
	
	private  Controller controller;

	/**
	 * Ezen a függvényen keresztül lehet jelezni, hogy  a Felhasználó valamiért veszített.
	 * Akkor hívódik, ha két vonat ütközik, vagy olyan vonat érkezik az ExitPointra ami még nincs kiürülve.
	 * @author rapos13
	 */
	
	public void gameLost()
	{
		gw.repaint();
		System.out.println("game lost");
		controller.gameOver(false);
		engineTimer.stop();
	}

	/**
	 * Ellenőrzi, hogy az EntryPoint-ok teljesítik - e a győzelmi feltételeket. (minden vonat kiürült.
	 * @author rapos13
	 */
	public boolean checkWinConditions()
	{
		boolean win = true;
		for (EntryPoint entryPoint : entryPoints) {
			if(entryPoint.isAllTrainEmpty() == false)
			{
				win = false;
			}
		}
		if(win)
		{
			gameWin();
		}
		
		return win;
	}

	/**
	 * Visszaad egy referenciát az egy darab EngineTimer objektumra.
	 * @return Referencia  az engineTimer-ra
	 * @author rapos13
	 */
	public EngineTimer getEngineTimer()
	{
		return engineTimer;	
	}

	/**
	 * A Játékos megnyerte a játékot. Akkor hívódik, ha az összes EntryPointhoz tartozó összes vonat üressé válik.
	 * @author rapos13
	 */
	public void gameWin()
	{
		engineTimer.stop();
		controller.gameOver(true);
	}

	/**
	 * Inicializálja a pályát a megadott paraméterben lefő fájl alapján.
	 * @param map pálya adatait tartalmazó file
	 * @author rapos13
	 */
	public void init(java.io.File map) 
	{
		gw.dropPreviousContent();
		
		if(engineTimer != null)
			engineTimer.stop();
		//ez az egy timer vezérli majd az össes vonatot
		engineTimer = new EngineTimer();
		//a timernek beállítjuk a gamewindow-t
		engineTimer.setGw(gw);
		
		controller.setEngineTimer(engineTimer);
		entryPoints.clear();
		
		//feldolgozzuk az xml fájlt
		InitXMLParser parser = new InitXMLParser(map);
		MapMeta mapMeta = new MapMeta();
		try {
			parser.parse(mapMeta);
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		//a létrejött elemeket eltároljuk, hogy később a kopcsolatokat be lehessen állítani
		Map<Position, Rail> rails = new HashMap<>();
		Map<Position, Switch> switches = new HashMap<>();
		Map<Position, CrossRail> cross = new HashMap<>();
		
		
		try {
			for ( StationMeta sm : mapMeta.stations) {
				
				Position position = sm.pos.toPosition();
				Station station = new Station(sm.stationColor, sm.passangers);
				station.setPosition(position);
				rails.put(position, station);
				
				gw.addStation(station);
			}
			
			for (RailMeta rm : mapMeta.rails) {
				Position position = rm.pos.toPosition();
				Rail rail = new Rail();
				rail.setPosition(position);
				rails.put(position, rail);
				
				// a view-nak beállítjuk a model elemet
				gw.addRail(rail);
				
			}
			
			
			for (SwitchMeta sm : mapMeta.switches) {
				Position position = sm.pos.toPosition();
				Switch sw = new Switch();
				sw.setPosition(position);
				switches.put(position, sw);
				controller.addSwitch(sw);
				
				gw.addSwitch(sw);
			}
			
			for(TunnelEntranceMeta tem : mapMeta.tunnelEntrances)
			{
				Position position = tem.pos.toPosition();
				TunnelEntrance te = new TunnelEntrance();
				te.setPosition(position);
				rails.put(position, te);
				controller.addTunnelEntrance(te);
				
				gw.addTunnel(te);
			}
			TunnelEntrance.setBuiltEntrances(0);
			
			for(ExitPointMeta exm : mapMeta.exitPoints)
			{
				Position position = exm.pos.toPosition();
				ExitPoint ex = new ExitPoint();
				ex.setPosition(position);
				rails.put(position, ex);
				
				gw.addExit(ex);
			}
			
			for(CrossRailMeta crm : mapMeta.crossRails)
			{
				Position position = crm.pos.toPosition();
				CrossRail cr = new CrossRail();
				cr.setPosition(position);
				cross.put(position, cr);
				
				gw.addCross(cr);
			}
			
			//EntryPointok létrehozása
			for(EntryPointMeta epm : mapMeta.entryPoints)
			{
				Position position = epm.pos.toPosition();
				EntryPoint ep = new EntryPoint(this, epm);
				ep.setPosition(position);
				rails.put(position, ep);
				entryPoints.add(ep);
				
				gw.addEntry(ep);
			}
			
			//Kapcsolatok beállítása
			
			Map<Position, Rail> allElement = new HashMap<>();
			allElement.putAll(rails);
			allElement.putAll(switches);
			allElement.putAll(cross);
			
			for(Connectable c : mapMeta.getAllConns())
			{	
				Position n1 = null;
				Rail n1r = null;
				
				Position n2 = null;
				Rail n2r = null;
				
				Position rp = null;
				Rail rail = null;
				
				n1 = c.getConnections().get(0).to().toPosition();
				n1r = allElement.get(n1);
				
				rp = c.getConnections().get(0).from().toPosition();
				rail = allElement.get(rp);
				
				if(c.getConnections().size() > 1)
				{
					n2 = c.getConnections().get(1).to().toPosition();
					n2r = allElement.get(n2);
				}
				
				rail.setNeighbourRails(n1r, n2r);
			}
			
			for(Connectable c : mapMeta.switchConns())
			{
				Position sp = c.getConnections().get(0).from().toPosition();
				Switch sw = switches.get(sp);
				
				Position thirdRailPos = c.getConnections().get(2).to().toPosition();
				Rail thirdRail = allElement.get(thirdRailPos);
				sw.setThirdRail(thirdRail);
			}
			
			for(Connectable c : mapMeta.crossConns())
			{
				Position crp = c.getConnections().get(0).from().toPosition();
				CrossRail cr = cross.get(crp);
				
				Position n3 = c.getConnections().get(2).to().toPosition();
				Position n4 = c.getConnections().get(3).to().toPosition();
				
				Rail r3 = allElement.get(n3);
				Rail r4 = allElement.get(n4);
				
				cr.setOtherRails(r3, r4);
			}
			
			//init végén megjelenítjük az ablakot // majd a game window tudja mikor
			gw.initFinished();
			//gw.showWindow();
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		//engineTimer.start();
		
	}

	/**
	 * 
	 * @param controller
	 * @author rapos13
	 */
	public void setController(Controller controller)
	{
		this.controller = controller;
	}

	public void setView(GameWindow gameWindow) {
		gw = gameWindow;
		
	}

}