package model;

import java.io.Serializable;
import java.util.*;

import parser.CarriageMeta;
import parser.EngineMeta;
import parser.EntryPointMeta;

/**
 * Megkapja az információt, hogy a vonat épp baleset miatt vesztést hirdet vagy éppen kiürült.
 */
public class EntryPoint extends Rail implements Serializable
{

	/**
	 * Referenci a TableModellre. Azért kell, hogy az EntryPoint tudjon kommunikálni a TableModellel (győzelem, baleset, vereség)
	 */
	private TableModel tableModel;
	/**
	 * Az entryPointhoz tartozó Engine objektumok listája.
	 */
	private ArrayList<Engine> engineList;
	/**
	 * Számolja a nem üres vonatokat. Tehát, he egy vonat kiürül, akkor csökkenti a számlálót. Kezdetben az értéke annyi, ahány vonat tartozik az EntryPointhoz.
	 */
	private int notEmptyTrainsCount;
	/**
	 * Indulásig itt várakoznak a Carriage Objektumok.
	 */
	private ArrayList<Carriage> carriageList;
	

	/**
	 * Paraméterben megkapja a TableModel referenciáját is, hogy vissza tudjon neki szólni, ha kell (pl.: baleset történt).
	 * @param tm A TableModel referenciája.
	 * @param epm A létrehozáshoz szükséges információk. További részletekért lásd az EntryPointMeta dokumentációját
	 */
	public EntryPoint(TableModel tm, EntryPointMeta epm)
	{
		tableModel = tm;
		engineList = new ArrayList<Engine>();
		carriageList = new ArrayList<Carriage>();		
		EngineTimer et = tableModel.getEngineTimer();
		for(EngineMeta em : epm.engines) // Minden Enginere
		{
			Engine e = new Engine(this, em.startDelay, et, em.engineColor, em.empty, this);
			
			// EZÉRT AGYBA FŐBE VERÉS
			//et.registerEngine(e); //regisztrálja az időzítőre
			
			for(CarriageMeta cm : em.carriages) //Az engine hez tartozó carriage-ekre
			{
				Carriage nc = null;
				if(cm.isCoal) // ha csak szenet szállít
				{
					nc = new CoalWagon(e, this);
				}
				else
				{
					nc = new Carriage(cm.carriageColor, cm.empty, e, this);
					
				}
				// EZÉRT AGYBA FŐBE VERÉS TODO kitörölni
				// e.addCarriage(nc); // hozzáadja az engine-hez
				carriageList.add(nc); //eltárolja
			}
			engineList.add(e); // a kész engine-t eltárolja
			
		}
		
	}

	/**
	 * Az hozzatratozó mozdonyok ezen a függvényen keresztül jelzik az EntryPointnak, hogy kiürültek. Ezt az EntryPoint jegyzi magának.
	 * @param engine Melyik mozdony hirdette, hogy kiürült.
	 */
	public void trainBecameEmpty(Engine engine)
	{
		
		tableModel.checkWinConditions();
		
	}

	/**
	 * Baleset hirdetése az EntryPointnak.
	 */
	public void announceAccident()
	{
		tableModel.gameLost();
	}

	/**
	 * Ellenőrzi, hogy az összes vonat üres-e.
	 */
	public boolean isAllTrainEmpty()
	{
		//return (notEmptyTrainsCount == 0);
		boolean ret = true;
		for(Engine e : engineList)
		{
			if(!e.isTrainEmpty())
				ret = false;
		}
		return ret;
	}
}