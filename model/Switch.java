package model;

public class Switch extends Rail
{

	/**
	 * A váltó harmadik végpontja.
	 */
	private Rail otherRail;
	
	
	
	// belső működéshez tartozó változók
	
	State [] states = {new AB(),new BC(), new CA()};
	int currentState = 0;
	/**
	 * Ellenőrzi hogy van-e Carriage a Switch-en, ha nincs akkor végrehajtja a váltást.
	 */
	public void switchOutput()
	{
		rotate();
	}

	/**
	 * Átállítja a váltót.
	 */
	private void rotate()
	{
		if(!isObstacleOnRail()){
			currentState = (currentState+1)%3;
			controller.Controller.print("new state: " + states[currentState].getName());
		}else{
			controller.Controller.print("Switch is currently occupied, cannot switch");
		}
	}

	/**
	 * Konstruktor.
	 */
	public Switch()
	{
		super();
		otherRail = null;
	}

	/**
	 * Beállítja a harmadik kimenetet.
	 * @param rail A harmadik kimenet.
	 */
	public void setThirdRail(Rail rail)
	{
		otherRail = rail;
	}

	/**
	 * Amikor egy Carriage objektum elhagyja a RAil objektumok, akkor ezt a függvényt hívja. A Rail objektumban törli a referenciát az aktuális Carriage objektumra.
	 */
	public Rail registerOut()
	{
		actualCarriage = null;
		return states[currentState].whereToSend();
	}
	@Override
	public void registerIn(Carriage carriage, Rail from) throws AccidentException
	{
		states[currentState].trainArrived(from);
		super.registerIn(carriage, from);
	}
	
	
	public Rail getNeighbour3()
	{
		return otherRail;
	}
	
	public int getState()
	{
		return currentState;
	}
	
	
	// belső működéshez tartozó osztályok
	private abstract class State{
		public Rail whereToSend(){
			return null;
		}
		public void trainArrived(Rail from) throws AccidentException{}
		public abstract String getName();
	}
	private class AB extends State{
		@Override
		public Rail whereToSend(){
			if(from == neighbour1)
				return neighbour2;
			else
				return neighbour1;
		}
		@Override
		public void trainArrived(Rail from) throws AccidentException{
			if(from == otherRail)
				throw new AccidentException();
		}
		@Override
		public String getName() {
			return "AB";
		}
	}
	private class BC extends State{
		@Override
		public Rail whereToSend(){
			if(from == neighbour2)
				return otherRail;
			else
				return neighbour2;
		}
		@Override
		public void trainArrived(Rail from) throws AccidentException{
			if(from == neighbour1)
				throw new AccidentException();
		}
		@Override
		public String getName() {
			return "BC";
		}
	}
	private class CA extends State{
		@Override
		public Rail whereToSend(){
			if(from == neighbour1)
				return otherRail;
			else
				return neighbour1;
		}
		@Override
		public void trainArrived(Rail from) throws AccidentException{
			if(from == neighbour2)
				throw new AccidentException();
		}
		@Override
		public String getName() {
			return "CA";
		}
	}


}