package model;

import java.util.*;

public class Station extends Rail
{

	/**
	 * az állomás színe
	 */
	private Color color;
	public Color getColor() {
		return color;
	}

	public ArrayList<Color> getPassengers() {
		return passengers;
	}

	/**
	 * Az állomáson várakozó utasok színei.
	 */
	private ArrayList<Color> passengers;

	/**
	 * Konstruktor. Létrehozza  az állomást a megadott színnel és felszálló utasokkal.
	 * @param c Az állomás színe.
	 * @param passangers A felszállásra várakozó utasok.
	 */
	public Station(Color c, List<Color> passangers)
	{
		color = c;
		this.passengers = (ArrayList<Color>) passangers;
	}

	/**
	 * Szól a rajta levő Carriage-nek, hogy szájjanak le róla ha tudnak. Ehhez megmondja a Carriage-nek hogy milyen színű állomáson van.
	 * @param carriage Az aktuális vonatelem, amely ráhajt a sínre.
	 * @param from A sín ahonnan ráhajtanak.
	 * @throws AccidentException 
	 */
	@Override
	public void registerIn(Carriage carriage, Rail from) throws AccidentException
	{
		super.registerIn(carriage, from);
		carriage.carriageOnStation(this, color);
	}

	/**
	 * Felszállítja az utasokat.
	 * @param carriageColor A kocsi színe.
	 * @return Értéke true, ha az állomásról szálltak fel utasok a kocsira.
	 */
	public boolean getIn(Color carriageColor)
	{
		if(passengers.contains(carriageColor))
		{
			passengers.remove(carriageColor);
			return true;
		}
		return false;
	}
	
}