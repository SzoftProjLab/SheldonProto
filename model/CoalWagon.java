package model;

public class CoalWagon extends Carriage
{

	// Nem csinál semmit.
	public void carriageOnStation(Color stationColor)
	{
		return;
	}

	/**
	 * 
	 * @param engine
	 */
	public CoalWagon(Engine engine, Rail actualRail)
	{
		//generated code
		super(null, true, engine, actualRail);
	}
	
	@Override
	public String toString() {
		return "CoalWagon";
	}
	
	@Override
	public boolean isEmpty()
	{
		return false;
	}

}