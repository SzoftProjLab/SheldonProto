package model;

public class Position
{
	@Override
	public String toString() {
		return "Position [posX=" + posX + ", posY=" + posY + "]";
	}

	/**
	 * X koordináta.
	 */
	public int posX;
	
	/**
	 * Y koordináta.
	 */
	public int posY;

	/**
	 * Konstruktor, paramétereiben átvett értékeket beállítja.
	 * @param posX
	 * @param posY
	 */
	public Position(int posX, int posY)
	{
		this.posX = posX;
		this.posY = posY;
	}
	
	/**
	 * Összehasonlító operátor, megvizsgálja, hogy a két pozíció egyezik-e.
	 * @param obj Az összehasonlítandó pozíció.
	 * @return True: A két pozíció ugyan az, False: A két pozíció különböző.
	 */
	@Override
	public boolean equals(Object obj)
	{
		Position p = (Position) obj;
		return (p.posX == posX && p.posY == posY);
	}
	
	/**
	 * Hash kód generáló.
	 * @return A generált hash kód.
	 */
	 @Override
	    public int hashCode() {
	        int prime1 = 2;
	        int prime2 = 3;
	        
	        return  (int) (java.lang.Math.pow(prime1, posX) * java.lang.Math.pow(prime2, posY));
	    }
	
}