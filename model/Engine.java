package model;

/**
 * A vonat első eleme, egy speciális Carriage. Időközönként előre mozdul, és "húzza magával" a mögötte levő Carriage-ket. Lehet neki kezdeti késleltetése.
 */
public class Engine extends Carriage
{
	/**
	 * Referencia az engineTimer-ra.
	 */
	private EngineTimer timer;
	private EntryPoint entryPoint;

	/**
	 * Konstruktor.
	 * @param ep Melyik EntryPoint-hoz tartozik. A mozdonynak ismerie kell hiszen ezen keresztül jelzi vissza a baleseteket és azt, ha kiürült a vonat.
	 * @param startTimeDelay Mennyi idő múlva lép a vonat legelőször.
	 * @param engineTimer Létrehozáskor az EntryPoint elkéri a TableModel-től és a beállítja az Engine-nek.
	 * @param c a vonat színe
	 * @param empty true: üres kocsi
	 * @param actualRail
	 */
	public Engine(EntryPoint ep, int startTimeDelay, EngineTimer engineTimer, Color c, boolean empty, Rail actualRail)
	{
		//generated code
		super(c, empty, null, actualRail);
		
		this.entryPoint = ep;
		this.timer = engineTimer;
		super.startDelay = startTimeDelay;
		
		engineTimer.registerEngine(this);
		this.setAllPrevCarriageIsEmpty(true);
		this.myEngine = this;
	}

	/**
	 * Az utolsó kocsi hívja akkor, amikor kiürül.
	 * Szól a hozzá tartozó EntryPointnak, hogy a vonat kiürült.
	 */
	public void trainBecomeEmpty()
	{
		entryPoint.trainBecameEmpty(this);
	}

	/**
	 * Az engineTimer hívja timeOut-kor.
	 */
	public void timeOut()
	{
		moveFurther();
	}

	/**
	 * 
	 * @param newCar
	 */
	@Override
	public void addCarriage(Carriage newCar)
	{
		super.addCarriage(newCar);
	}

	@Override
	protected void announceAccident()
	{
		removeTimer();
		entryPoint.announceAccident();
	}

	/**
	 * A vonatot kiveszi az engineTimer vezérlési listájából.
	 * Ehhez szól az EngineTimernek, hogy ezt a mozdonyt vegye ki a fenntartott kollekcióból.
	 */
	@Override
	protected void removeTimer()
	{
		timer.removeEngine(this);
	}

}