package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import view.GameWindow;

public class EngineTimer
{

	GameWindow gw = null;

	public void setGw(GameWindow gw) {
		this.gw = gw;
	}

	/**
	 * a vezérelendő vonatok listája
	 */
	private List<Engine> engineList = new ArrayList<>();

	/**
	 * ctor
	 * @author rapos13
	 */
	
	
	/**
	 * elindítja a léptető ciklust.
	 * @author rapos13
	 */
	public void start()
	{
		//minden indításkor létrehozunk egy új timert, daemon szállal
		//előtte ha futna, akkor cancellájuk
		if(timer != null)
		timer = null;
		timer = new Timer(true);
		
		timer.schedule(new EnginNotifier(), 700, 700);
	}

	/**
	 * Hozzáad egy Engine-t a vezérelendő vonatok listájához.
	 * @param engine a vezérelendő Engine
	 * @author rapos13
	 */
	public void registerEngine(Engine engine)
	{
		engineList.add(engine);
	}

	/**
	 * Eltávolítja a paraméterként kapott Engine-t az engineList-ből.
	 * @param engine az eltávolítandó Engine
	 * @author rapos13
	 */
	public void removeEngine(Engine engine)
	{
		engineList.remove(engineList.indexOf(engine));
	}

	/**
	 * A manuális tick-kor hívja meg majd rajta a Controller osztály. Az összes vonatnak meghívja a timeout() fv-ét.
	 * @author rapos13
	 */
	public void tick()
	{
		
		for(int i = 0; i < engineList.size(); ++i)
		{
			engineList.get(i).timeOut();
			
		}
		gw.repaint();
	}

	/**
	 * Befejezi a léptető ciklust.
	 * @author rapos13
	 */
	public void stop()
	{
		if(timer != null)
			timer.cancel();
		timer = null;
	}
	
	//--------------------------------------Privát dolgok-----------------
	
	//egy timer
	private  Timer timer;
	
	
	/**
	 * ezt a feladatot hajtja végre
	 * @author rapos13
	 *
	 */
	private class EnginNotifier extends TimerTask
	{

		@Override
		public void run() {
			tick();			
		}
		
	}

	

}