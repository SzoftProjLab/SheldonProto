package model;

public class CrossRail extends Rail
{

	/**
	 * A neighbour4-ről érkező kocsi ide megy tovább.
	 */
	private Rail neighbour3;
	/**
	 * A neighbour3-ról érkező kocsi ide megy tovább.
	 */
	private Rail neighbour4;

	/**
	 * Amikor egy Carriage objektum elhagyja a RAil objektumok, akkor ezt a függvényt hívja. A Rail objektumban törli a referenciát az aktuális Carriage objektumra.
	 */	
	@Override
	public Rail registerOut()
	{
		actualCarriage = null;
		Rail seged = from;
		from = null;
		if(seged == neighbour1)
			return neighbour2;
		else if(seged ==neighbour2)
			return neighbour1;
		else if(seged == neighbour3)
			return neighbour4;
		else
			return neighbour3;
	}

	/**
	 * Beállítja a további két szomnszédos síntelemet.
	 * @param rail3
	 * @param rail4
	 */
	public void setOtherRails(Rail rail3, Rail rail4)
	{
		neighbour3 = rail3;
		neighbour4 = rail4;
	}

	/**
	 * Default konstruktor
	 */
	public CrossRail()
	{
		super();
		neighbour3 = neighbour4 = null;
	}

}