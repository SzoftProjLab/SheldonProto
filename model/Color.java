package model;




public enum Color
{
	RED,
	BLUE,
	YELLOW,
	GREEN,
	BLACK;
	
	public java.awt.Color toAwtColor()
	{
		switch (this) {
		case RED:
			return java.awt.Color.RED;
		case BLUE:
			return java.awt.Color.BLUE;
		case YELLOW:
			return  java.awt.Color.YELLOW;
		case GREEN:
			return  java.awt.Color.GREEN;
		case BLACK:
			return  java.awt.Color.BLACK;
			

		default: return null;
			
		}
	}
	
}
