package parser;

import java.util.ArrayList;
import java.util.List;

/**
 * entryPoint metadata
 * @author rapos13
 *
 */
public class EntryPointMeta implements Connectable {

	public Pos pos; // Az elem pozíciója
	public Pos n1; // neki csak egy szomszédja van
	public List<EngineMeta> engines = new ArrayList<>(); // az entryPointhoz tartozó vonatok adatai
	
	/**
	 * ctor
	 * @param pos
	 * @param n1
	 */
	public EntryPointMeta(Pos pos, Pos n1) {
		super();
		this.pos = pos;
		this.n1 = n1;
	}

	/**
	 * debughoz
	 */
	@Override
	public String toString() {
		return " \n\t EntryPointMeta [pos=" + pos + ", n1=" + n1 + ", engines=" + engines + "]";
	}

	/**
	 * visszaadja a kapcsolatokat
	 */
	@Override
	public ArrayList<Connection> getConnections() {
		ArrayList<Connection> connections = new ArrayList<>();
		connections.add(new Connection(pos, n1));
		
		
		return connections;
	}
	
}
