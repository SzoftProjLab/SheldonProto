package parser;

import model.Position;

/**
 * Pozíciót leíró osztály
 * @author rapos13
 *
 */
public class Pos {
	public int x;
	public int y;
	
	/**
	 * ctor
	 * @param x
	 * @param y
	 */
	public Pos(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	/**
	 * debughoz
	 */
	@Override
	public String toString() {
		return "Pos [x=" + x + ", y=" + y + "]";
	}
	
	/**
	 * koverzio a model.Position osztályra
	 * @return
	 */
	public Position toPosition()
	{
		return new Position(x, y);
		
	}
	
}
