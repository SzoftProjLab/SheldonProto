package parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * segédalkalmazás. Egy mapot leíró xml file-t tölt be és kiirja  ametaadatokat
 * @author rapos13
 *
 */
public class MapViewer {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter map path: ");
        String s = br.readLine();
        InitXMLParser parser = new InitXMLParser(new File(s));
        MapMeta map = new MapMeta();
		try {
			parser.parse(map);
			System.out.println(map);
		} catch (Exception e) {
			System.out.println("WRONG MAP!");
			e.printStackTrace();
		}

	}

}
