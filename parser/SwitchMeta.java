package parser;

import java.util.ArrayList;

/**
 * Switch metaadatit tartalmazó osztály
 * @author rapos13
 *
 */
public class SwitchMeta extends RailMeta implements Connectable{
	
	/**
	 *harmadik szomszéd pozíciója
	 */
	public Pos n3;

	/**
	 * ctor
	 * @param pos
	 * @param n1
	 * @param n2
	 * @param n3
	 */
	public SwitchMeta(Pos pos, Pos n1, Pos n2, Pos n3) {
		super(pos, n1, n2);
		this.n3 = n3;
	}

	/**
	 * debughoz
	 */
	@Override
	public String toString() {
		return "\n\t SwitchMeta [n3=" + n3 + ", pos=" + pos + ", n1=" + n1 + ", n2=" + n2 + "]";
	}
	
	/**
	 * a switchez tartotó 3 db kapcsolatot adja vissza
	 */
	@Override
	public ArrayList<Connection> getConnections() {
		ArrayList<Connection> connections = super.getConnections();
		connections.add(new Connection(pos, n3));
		return connections;
	}
	

}
