package parser;

import java.util.ArrayList;
import java.util.List;

import model.Color;

/**
 * Station metaadatait tartalmazó sztály
 * @author rapos13
 *
 */
public class StationMeta extends RailMeta {

	/**
	 * állomás színe
	 */
	public Color stationColor;
	
	/**
	 * állomáson várakozó utasok
	 */
	public List<Color> passangers = new ArrayList<>();
	
	/**
	 * ctor
	 * @param pos
	 * @param n1
	 * @param n2
	 * @param stationColor
	 */
	public StationMeta(Pos pos, Pos n1, Pos n2, Color stationColor) {
		super(pos, n1, n2);
		this.stationColor = stationColor;
	}

	/**
	 * debughoz
	 */
	@Override
	public String toString() {
		String p = new String();
		for ( Color pc : this.passangers) {
			p += pc.toString();
			p+= " ";
		}
		return "\n\t StationMeta [stationColor=" + stationColor + ", passangers=" + p + ", pos=" + pos + ", n1="
				+ n1 + ", n2=" + n2 + "]";
	}

	
	

}
