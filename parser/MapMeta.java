package parser;

import java.util.ArrayList;
import java.util.List;

/**
 * A map adatit tartalmazó osztály. ezekből az információkból kell létrehozni a map-et
 * @author rapos13
 *
 */
public class MapMeta {
	public List<RailMeta> rails = new ArrayList<>();
	public List<CrossRailMeta> crossRails = new ArrayList<>();
	public List<SwitchMeta> switches = new ArrayList<>();
	public List<TunnelEntranceMeta> tunnelEntrances = new ArrayList<>();
	public List<ExitPointMeta> exitPoints = new ArrayList<>();
	public List<StationMeta> stations = new ArrayList<>();
	public List<EntryPointMeta> entryPoints = new ArrayList<>();
	
	/**
	 * 
	 * @return az összes előforduló kapcsolat 
	 */
	public List<Connectable> getAllConns()
	{
		
		List<Connectable> conns = new ArrayList<>();
		conns.addAll(rails);
		conns.addAll(tunnelEntrances);
		conns.addAll(exitPoints);
		conns.addAll(stations);
		conns.addAll(entryPoints);
		conns.addAll(crossRails);
		conns.addAll(switches);
		return conns;
	}
	/**
	 * 
	 * @return az összes switch kapcsolatai
	 */
	public List<Connectable> switchConns()
	{
		List<Connectable> conns = new ArrayList<>(switches);
		return conns;
	}
	
	/**
	 * 
	 * @return a crossrailek kapcsolatai
	 */
	public List<Connectable> crossConns()
	{
		List<Connectable> conns = new ArrayList<>(crossRails);
		return conns;
	}
	
	
	/**
	 * debughoz
	 */
	@Override
	public String toString() {
		return "MapMeta [ \n rails=" + rails + "\n crossRails=" + crossRails + "\n switches=" + switches
				+ "\n tunnelEntrances=" + tunnelEntrances + "\n exitPoints=" + exitPoints + "\n stations=" + stations
				+ "\n entryPoints=" + entryPoints + "]";
	}
	
	
	
	
}
