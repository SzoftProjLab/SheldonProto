package parser;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import model.Color;


/**
 * a mapet leíró xml file beolvasása és értelmezése
 * @author rapos13
 *
 */
public class InitXMLParser {

	File from;

	/**
	 * konstruktor
	 * @param from a beolvasandó fájl
	 */
	public InitXMLParser(File from) 
	{
		super();
		this.from = from;
	}
	/**
	 * Betölti az XML fájlt
	 * 
	 * @param map  a betöltendő xml file
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @author rapos13
	 */
	public void parse(MapMeta map)throws ParserConfigurationException,	SAXException, IOException 
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		// Load the input XML document, parse it and return an instance of the
		// Document class.
		Document document = builder.parse(from);
		
		NodeList nodeList = document.getDocumentElement().getChildNodes();
		
		
		for (int i = 0; i < nodeList.getLength(); i++) {
		
			Node node = nodeList.item(i);
			
			//RailMeta objektumok beolvasása
			if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName() == "Rails") {
				Element Rails = (Element) node;
				NodeList railList = Rails.getChildNodes();
				for(int r = 0; r < railList.getLength(); r++)
				{
					Node railNode = railList.item(r);
					if (railNode.getNodeType() == Node.ELEMENT_NODE && railNode.getNodeName() == "rail")
					{
						//az aktuális rail elem
						Element ra = (Element)railNode;
						
						//az aktuális rail pozíciója						
						Pos pos = getPos(ra);
						//és szomszédai
						Pos n1 = getNeighbour(ra, "n1");
						Pos n2 = getNeighbour(ra, "n2");
						
						map.rails.add( new RailMeta(pos, n1, n2));						
					}					
					
				}
			}
			//CrossRailMeta elemek beolvasása
			if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName() == "CrossRails") {
				Element Rails = (Element) node;
				NodeList railList = Rails.getChildNodes();
				for(int r = 0; r < railList.getLength(); r++)
				{
					Node railNode = railList.item(r);
					if (railNode.getNodeType() == Node.ELEMENT_NODE && railNode.getNodeName() == "crossRail")
					{
						//az aktuális rail elem
						Element cra = (Element)railNode;
						
						//az aktuális rail pozíciója						
						Pos pos = getPos(cra);
						//és szomszédai
						Pos n1 = getNeighbour(cra, "n1");
						Pos n2 = getNeighbour(cra, "n2");
						Pos n3 = getNeighbour(cra, "n3");
						Pos n4 = getNeighbour(cra, "n4");
						
						map.crossRails.add( new CrossRailMeta(pos, n1, n2, n3, n4));						
					}					
					
				}
			}
			
			//SwitchMeta elemek beolvasása
			if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName() == "Switches") {
				Element Rails = (Element) node;
				NodeList railList = Rails.getChildNodes();
				for(int r = 0; r < railList.getLength(); r++)
				{
					Node railNode = railList.item(r);
					if (railNode.getNodeType() == Node.ELEMENT_NODE && railNode.getNodeName() == "switch")
					{
						//az aktuális rail elem
						Element sw = (Element)railNode;
						
						//az aktuális rail pozíciója						
						Pos pos = getPos(sw);
						//és szomszédai
						Pos n1 = getNeighbour(sw, "n1");
						Pos n2 = getNeighbour(sw, "n2");
						Pos n3 = getNeighbour(sw, "n3");
						
						map.switches.add( new SwitchMeta(pos, n1, n2, n3));						
					}					
					
				}
			}	
			
			//TunnelEntrace elemek beolvasása
			if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName() == "TunnelEntrances") {
				Element Rails = (Element) node;
				NodeList railList = Rails.getChildNodes();
				for(int r = 0; r < railList.getLength(); r++)
				{
					Node railNode = railList.item(r);
					if (railNode.getNodeType() == Node.ELEMENT_NODE && railNode.getNodeName() == "tunnelEntrance")
					{
						//az aktuális rail elem
						Element te = (Element)railNode;
						
						//az aktuális rail pozíciója						
						Pos pos = getPos(te);
						//és szomszédai
						Pos n1 = getNeighbour(te, "n1");
						
						map.tunnelEntrances.add( new TunnelEntranceMeta(pos, n1));						
					}					
					
				}
			}	
			
			//ExitPoint elemek beolvasása
			if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName() == "ExitPoints") {
				Element Rails = (Element) node;
				NodeList railList = Rails.getChildNodes();
				for(int r = 0; r < railList.getLength(); r++)
				{
					Node railNode = railList.item(r);
					if (railNode.getNodeType() == Node.ELEMENT_NODE && railNode.getNodeName() == "exitPoint")
					{
						//az aktuális rail elem
						Element ex = (Element)railNode;
						
						//az aktuális rail pozíciója						
						Pos pos = getPos(ex);
						//és szomszédai
						Pos n1 = getNeighbour(ex, "n1");						
						
						map.exitPoints.add( new ExitPointMeta(pos, n1));						
					}					
					
				}
			}	
			
			//Station elemek beolvasása
			if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName() == "Stations") {
				Element Rails = (Element) node;
				NodeList railList = Rails.getChildNodes();
				for(int r = 0; r < railList.getLength(); r++)
				{
					Node railNode = railList.item(r);
					if (railNode.getNodeType() == Node.ELEMENT_NODE && railNode.getNodeName() == "station")
					{
						//az aktuális rail elem
						Element st = (Element)railNode;
						
						//az aktuális rail pozíciója						
						Pos pos = getPos(st);
						//és szomszédai
						Pos n1 = getNeighbour(st, "n1");
						Pos n2 = getNeighbour(st, "n2");	
						
						Color stColor = Color.valueOf(st.getElementsByTagName("stationColor").item(0).getChildNodes().item(0).getNodeValue());
						
						StationMeta stm =  new StationMeta(pos, n1, n2, stColor);
						
						
						NodeList Passangers = ((Node) st.getElementsByTagName("Passangers").item(0)).getChildNodes();
						
						for(int p = 0; p < Passangers.getLength(); ++p)
						{
							Node passanger = Passangers.item(p);
							if(passanger.getNodeType() == Node.ELEMENT_NODE && passanger.getNodeName() == "passanger")
							{
								Element pss = (Element) passanger;
								Color pColor = Color.valueOf(pss.getElementsByTagName("color").item(0).getChildNodes().item(0).getNodeValue());
								stm.passangers.add(pColor);
							
							}
						}
						map.stations.add(stm);
					}					
					
				}
			}	
			
			//EntryPoint elemek beolvasása
			if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName() == "EntryPoints") {
				Element Rails = (Element) node;
				NodeList railList = Rails.getChildNodes();
				for(int r = 0; r < railList.getLength(); r++)
				{
					Node railNode = railList.item(r);
					if (railNode.getNodeType() == Node.ELEMENT_NODE && railNode.getNodeName() == "entryPoint")
					{
						//az aktuális entrypoint elem
						Element ep = (Element)railNode;
						
						//az aktuális elem pozíciója						
						Pos pos = getPos(ep);
						//és szomszédai
						Pos n1 = getNeighbour(ep, "n1");						
						
						EntryPointMeta epm	= new EntryPointMeta(pos, n1);
						
						//az entrypointhoz tartozó engine -k
						NodeList Engines = ((Node) ep.getElementsByTagName("Engines").item(0)).getChildNodes();
						
						//minden engine- t beolvasunk
						for(int e = 0; e < Engines.getLength(); ++e)
						{
							Node enginNode = Engines.item(e);
							if(enginNode.getNodeType() == Node.ELEMENT_NODE && enginNode.getNodeName() == "engine")
							{
								Element eng = (Element)enginNode;
								//engine color
								Color engCol = Color.valueOf(eng.getElementsByTagName("color").item(0).getChildNodes().item(0).getNodeValue());
								//üres-e
								//start delay
								boolean empty = Boolean.parseBoolean(eng.getElementsByTagName("empty").item(0).getChildNodes().item(0).getNodeValue());
								int startDelay = Integer.parseInt(eng.getElementsByTagName("startDelay").item(0).getChildNodes().item(0).getNodeValue());
								
								//elkezjük beolvasni az engine-hez tartozó carriageket és coalwagonokat
								EngineMeta engMeta = new EngineMeta(engCol, empty, startDelay);
								
								Node start = eng.getElementsByTagName("startDelay").item(0);
								Node carriage = start.getNextSibling();
								//amíg van beolvasandó kocsi/szeneskocsi
								while(carriage != null)
								{
									if(carriage.getNodeType() == Node.ELEMENT_NODE && carriage.getNodeName() == "coalwagon")
									{
										engMeta.carriages.add(new CoalMeta());
									}
									else if( carriage.getNodeType() == Node.ELEMENT_NODE && carriage.getNodeName() == "carriage")
									{
										Element carr = (Element)carriage;
										Color carriageColor = Color.valueOf(carr.getElementsByTagName("color").item(0).getChildNodes().item(0).getNodeValue());
										boolean carrEmpty = Boolean.parseBoolean(carr.getElementsByTagName("empty").item(0).getChildNodes().item(0).getNodeValue());
										engMeta.carriages.add(new CarriageMeta(carriageColor, carrEmpty));
									}				
									
									carriage = carriage.getNextSibling();
								}
								epm.engines.add(engMeta);
							}
						}
						map.entryPoints.add(epm);
					}					
					
				}
			}	
		}
	}
	
	/**
	 * Adott elem pozícióját olvassa ki az xml-ből
	 * @param kinek melyik elem poziciója kell
	 * @return 
	 * @author rapos13
	 */
	private Pos getPos(Element kinek)
	{
		int posX = Integer.parseInt(kinek.getElementsByTagName("posX").item(0).getChildNodes().item(0).getNodeValue());
		int posY = Integer.parseInt(kinek.getElementsByTagName("posY").item(0).getChildNodes().item(0).getNodeValue());
		return new Pos(posX, posY);
	}
	
	/**
	 * Adott elem nek visszaadja a megadott szomszédját
	 * @param kinek
	 * @param melyik
	 * @return
	 */
	private Pos getNeighbour(Element kinek, String melyik)
	{
		return getPos(((Element)(kinek.getElementsByTagName(melyik).item(0))));
	}
}
