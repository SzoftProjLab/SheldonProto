package parser;

import java.util.ArrayList;

/**
 * ExitPoint matadata
 * @author rapos13
 *
 */
public class ExitPointMeta implements Connectable{
	public Pos pos;
	public Pos n1;
	/**
	 * ctor
	 * @param pos
	 * @param n1
	 */
	public ExitPointMeta(Pos pos, Pos n1) {
		super();
		this.pos = pos;
		this.n1 = n1;
	}

	/**
	 * debughoz
	 */
	@Override
	public String toString() {
		return "\n\t ExitPointMeta [pos=" + pos + ", n1=" + n1 + "]";
	}

	/**
	 * visszaadja a kapcsolatokat
	 */
	@Override
	public ArrayList<Connection> getConnections() {
		ArrayList<Connection> connections = new ArrayList<>();
		connections.add(new Connection(pos, n1));
		return connections;
	}
	
	

}
