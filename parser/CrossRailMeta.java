package parser;

import java.util.ArrayList;

/**
 * CrossRail metaadatai
 * @author rapos13
 *
 */
public class CrossRailMeta extends RailMeta implements Connectable {
	public Pos n3;
	public Pos n4;
	/**
	 * ctor
	 * @param pos
	 * @param n1
	 * @param n2
	 * @param n3
	 * @param n4
	 */
	public CrossRailMeta(Pos pos, Pos n1, Pos n2, Pos n3, Pos n4) {
		super(pos, n1, n2);
		this.n3 = n3;
		this.n4 = n4;
	}

	/**
	 * debug
	 */
	@Override
	public String toString() {
		return "\n\t CrossRailMeta [n3=" + n3 + ", n4=" + n4 + ", pos=" + pos + ", n1=" + n1 + ", n2=" + n2 + "]";
	}

	/**
	 * visszadja a kapcsolatokat
	 */
	@Override
	public ArrayList<Connection> getConnections() {
		ArrayList<Connection> connections = new ArrayList<>();
		connections.add(new Connection(pos, n1));
		connections.add(new Connection(pos, n2));
		connections.add(new Connection(pos, n3));
		connections.add(new Connection(pos, n4));
		
		return connections;
	}
	
	

}
