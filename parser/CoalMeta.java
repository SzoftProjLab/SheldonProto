package parser;

import model.Color;

/**
 * CoalWagon metadatok
 * @author rapos13
 *
 */
public class CoalMeta extends CarriageMeta {
	//neki nincs plussz attributuma, csak beállítja a coalwagonhoz tartozó default értékeket

	/**
	 * ctor
	 */
	public CoalMeta() {
		super(Color.BLACK, true);
		isCoal = true;
	}

	/**
	 * debughoz
	 */
	@Override
	public String toString() {
		return "CoalMeta [isCoal=" + isCoal + "]";
	}
	
	
}
