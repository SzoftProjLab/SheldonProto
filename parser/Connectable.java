package parser;

import java.util.ArrayList;
/**
 * Azok az elemek, melyek kapcsolódhatnak valamihez
 * @author rapos13
 *
 */
public interface Connectable {
	/**
	 * A kapcsolatokat adja vissza
	 * @return az elemhez tartozó kapcsolatok
	 */
	public ArrayList<Connection> getConnections();
}
