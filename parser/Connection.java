package parser;

/**
 * egy kapcsolatot reprezentáló osztály
 * @author rapos13
 *
 */
public class Connection {
	private Pos c1;
	private Pos c2;
	
	/**
	 * Ctor.
	 * @param c1 egyik vég
	 * @param c2 másik vég
	 */
	public Connection(Pos c1, Pos c2) {
		super();
		this.c1 = c1;
		this.c2 = c2;
	}
	
	/**
	 * honnan indul a kapcsolat
	 * @return
	 */
	public Pos from()
	{
		return c1;
	}
	
	/**
	 * hol van a kapcsolat vége?
	 * @return
	 */
	public Pos to()
	{
		return c2;
	}

}
