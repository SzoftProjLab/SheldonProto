package parser;

import java.util.ArrayList;
import java.util.List;

import model.Color;

/**
 * engine metaadatai
 * @author rapos13
 *
 */
public class EngineMeta  {
	
	public Color engineColor; // Engine színe
	public boolean empty; // Engine üres-e
	public int startDelay; // Késleltetési idő
	public List<CarriageMeta> carriages = new ArrayList<>(); // Engine-hez tartozó carriage-k listája, abban a sorrendben ahogy az xml-ben vannak
	
	/**
	 * ctor
	 * @param engineColor
	 * @param empty
	 * @param startDelay
	 */
	public EngineMeta(Color engineColor, boolean empty, int startDelay) {
		super();
		this.engineColor = engineColor;
		this.empty = empty;
		this.startDelay = startDelay;
	}

	//debugoláshoz
	@Override
	public String toString() {
		String carr = new String();
		for (CarriageMeta carriageMeta : carriages) {
			carr += carriageMeta.toString();
			carr += " :-: ";
		}
		return "EngineMeta [engineColor=" + engineColor + ", empty=" + empty + ", startDelay=" + startDelay
				+ ", carriages=" + carr + "]";
	}
	
	
}
