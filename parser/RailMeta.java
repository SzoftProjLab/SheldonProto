package parser;

import java.util.ArrayList;

/**
 * Rail metaadatit tartalmazó osztály
 * @author rapos13
 *
 */
public class RailMeta implements Connectable{

	public Pos pos;
	public Pos n1;
	public Pos n2;
	
	/**
	 * ctor
	 * @param pos
	 * @param n1
	 * @param n2
	 */
	public RailMeta(Pos pos, Pos n1, Pos n2) {
		super();
		this.pos = pos;
		this.n1 = n1;
		this.n2 = n2;
	}

	/**
	 * debughoz
	 */
	@Override
	public String toString() {
		return "\n\t RailMeta [pos=" + pos + ", n1=" + n1 + ", n2=" + n2 + "]";
	}

	/**
	 * a railhez tartozó kapcsolatokat adja vissza
	 */
	@Override
	public ArrayList<Connection> getConnections() {
		ArrayList<Connection> connections = new ArrayList<>();
		connections.add(new Connection(pos, n1));
		connections.add(new Connection(pos, n2));
		
		return connections;
	}
	
	
}