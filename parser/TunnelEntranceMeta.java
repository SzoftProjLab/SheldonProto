package parser;

import java.util.ArrayList;

/**
 * tunnelEntrance-hoz tartozó metaadatok
 * @author rapos13
 *
 */
public class TunnelEntranceMeta implements Connectable{
	
	/**
	 * pozíció
	 */
	public Pos pos;
	/**
	 * szomszed
	 */
	public Pos n1;
	
	/**
	 * ctor
	 * @param pos
	 * @param n1
	 */
	public TunnelEntranceMeta(Pos pos, Pos n1) {
		super();
		this.pos = pos;
		this.n1 = n1;
	}

	/**
	 * debughoz
	 */
	@Override
	public String toString() {
		return "\n\t TunnelEntranceMeta [pos=" + pos + ", n1=" + n1 + "]";
	}

	/**
	 * visszaadja a kapcsolatot
	 */
	@Override
	public ArrayList<Connection> getConnections() {
		ArrayList<Connection> connections = new ArrayList<>();
		connections.add(new Connection(pos, n1));
		
		
		return connections;
	}

	
}
