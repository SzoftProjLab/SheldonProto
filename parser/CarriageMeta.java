package parser;

import model.Color;

/**
 * Carriage metaadatai
 * @author rapos13
 *
 */
public class CarriageMeta {
	public Color carriageColor; //carriage színe
	public boolean empty; // üres-e
	public boolean isCoal; // Ugye az EngineMeta-ban Carriage lista van. Ezt a flaget kell ellenőrizni ahhoz, hogy eldöntsük hogy coalWagon vagy Carriage az aktuális elem. Lásd: CoalMeta doksija
	
	/**
	 * ctor
	 * @param carriageColor
	 * @param empty
	 */
	public CarriageMeta(Color carriageColor, boolean empty) {
		super();
		this.carriageColor = carriageColor;
		this.empty = empty;
		this.isCoal = false;
	}

	/**
	 * debughoz
	 */
	@Override
	public String toString() {
		return "CarriageMeta [carriageColor=" + carriageColor + ", empty=" + empty +  "]";
	}
	
	
	

}
