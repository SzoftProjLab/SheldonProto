package main;

import controller.Controller;
import model.TableModel;
import view.GameWindow;
import view.MenuPanel;
import view.MenuPanel.MenuType;

/**
 * @author Toth
 * A program belépési pontját tartalmazó osztály
 * Létrehozza a controller és tableModel objektumokat (a Controller, TableModel osztályokat példányosítva).
 * Felépíti az asszociációkat (controller és tableModel ismerik egymást).
 */
public class Application {

	/**
	 * A program belépési pontja
	 * @param args a program parancssori argumentumai
	 */
    public static void main(String[] args) {
    	
		Controller controller = new Controller();
		TableModel tableModel = new TableModel();
		GameWindow gameWindow = new GameWindow();
		tableModel.setView(gameWindow);
		
		
		controller.setTableModel(tableModel); 	// asszociációk beállítása
		tableModel.setController(controller);
		gameWindow.setController(controller);
		controller.setGameWindow(gameWindow);
		
		controller.play();
		
    }

}
