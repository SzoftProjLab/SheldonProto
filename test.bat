@ECHO OFF
IF EXIST "tests\test_%1_real.txt" (
	del tests\test_%1_real.txt
)
type tests\test_%1_stdin.txt | java main.Application >> tests\test_%1_real.txt
fc tests\test_%1_stdout.txt tests\test_%1_real.txt
del tests\test_%1_real.txt